## Categorical edge-based analyses of avian phylogenomic data. 

Author: Ning Wang, Edward L. Braun, Bin Liang, Joel Cracraft, Stephen A. Smith


## Choose potential edges to test and organize tree file  

### edges to test   

**Organizing clades**    
A: avian outgroup, Struthio and Galloanserea  
B (V): Nightjar + Swifts + Hummingbirds   
C (IV): Cuckoos + (C':Turacos + Bustards)  
D (VI): Doves + Sandgrouse + Mesites   
E (VII): Flamingos + Grebes  
F (II): Core waterbirds (Gavii and allies)  
G (III): Sunbittern + Tropicbirds  
*S*: Shorebirds (Plovers)   
*CR*: Cranes and Rails   
*H*: Hoazin  
**I**: core landbirds  
*EHV*: Eagles and Hawks  
*O*: Owls  
*M*: mousebirds (Colliforms)  
*W*: Woodpeckers and allies  
 

**Major References**  
Jarvis et al. 2014   
Prum et al. 2015  
Reddy et al. 2017   
McCormack et al. 2013  
Hackett et al. 2008  
Jetz et al. 2012  
Burleigh et al. 2015  
Suh et al. 2015
Shen et al. 2017 (just one test for basal clade)

**Three edges to test and their hypothesis**   

1. Basal of Neoaves  
	- **VI+VII**  (Jarvis TENT, Reddy)
	- **V**	  (Jarvis Exon12, Prum)
	- **VII**  (Jarvis UCE)
	- **III+V+VI+VII** (Hackett)
	- Doves (McCormack, also be tested in Shen paper).
	- five more addition (to test potential SLA effect)
		* Nightjar
		* Hoazin
		* Plover
		* Crane
		* Cuckoo
	
2. Hoazin position
	- *H*-**I+III+V**	(Reddy)
	- *H*-**I**	(Prum)
	- *H*-*S+CR*	(Jarvis TENT, Jarvis UCE)
	- *H*-**V**	(Jarvis Intron)
	- *H*-*S*	(McCormack et al, 416UCE, Jarvis Exon123)  
	- *H*-*TB* (Jarvis Exon12)
	- *H*-**II+III**+*S+CR* (Jarvis MP_EST*)

3. Owl position
	- *O*-*M+W*	(Reddy, Prum, Jarvis Intron)
	- *O*-*W*	(McCormack, Suh, Jarvis UCE)
	- *O*-*EHV*	(Jarvis Exon12)
	- *O*-*M*	(Hackett)

**testing data**

## Javis filtered Intron, UCE, and Exon data   

1. rename intron file name  
	`for fo in *; do mv $fo/sate.removed.intron.original.aligned-allgap.filtered $fo/$fo.intron; done`  
	* exons: 8295 loci
	* introns: 2516 loci
	* UCEs: 3679 loci	
2. filter out introns/exons having less than 500bp aligned seq, less than 42 species after delete sequences with len(nonalignseq)/len(alignseq) < 0.3   
	`python ../Code/Filter_data.py . .fasta filter0.3/ 0.3 500`  
	* this result in:
		* exons: 7371 loci
		* introns: 2270 loci
		* UCEs: 3679 loci	 
3. **run iqtree for each dataset with ultrafast bootstrap** (these unconstraint ML iqtrees will be used for later comparison)   
	`python ../Code/run_iqtree.py . 8`  
	
4. **clone gophy and set up godir**  (inorder to use bp2/bp to check outgroup postition)
	- in `~/Document` make a directory `godir`
	- within `godir` make `bin, pkg, src` directories
	- cd into `src`
	- `git clone https://github.com/FePhyFoFum/gophy`
	- `sudo apt install golang-go`
	- add `GOPATH` into ~/.bashrc
		`export GOPATH=/home/ningwum/Documents/godir`
	- `go get -u gonum.org/v1/gonum/...`  
		gonum can be used to build go conditions, which are required for the following call:
	- `go build gophy/bp2/bp2.go` (redo this step after `git pull`)
	- there will be a binary `bp2` in the `src` directory, which can be used for bp2 analyses. 
	- basic use of `bp2`:
		`bp2 -t MLtrees -comp constraint.tre -wks 10`   
	**for Mac book**
	- download and install golang first  `https://golang.org/doc/install?download=go1.11.darwin-amd64.pkg`
	- follow the steps and install
		- Download the package file, open it, and follow the prompts to install the Go tools. 
		- The package installs the Go distribution to `/usr/local/go`
	- reopen terminal and in home directory do: `go get github.com/FePhyFoFum/gophy`
	- cd into go/src directory and do: `go build github.com/FePhyFoFum/gophy/bp/bp.go`
	- put bp in /usr/local/bin
	- use `bp` to compare bipartition. 

5. check outgroup behavior for ML trees
	- `outgroup.tre` suppose to contain 1 bipartition
	- `python PATH/Code/check_outgroup_mono.py outgrouptree DIR file_end cutoff`
	- double check `check_outgroup_mono.py` before run    
	**For intron and exon** (similar filter step for exon)
	- **outgroup4 (non\_bird four outgroups) and outgroup9 (four non\_bird + Palaeognathae + Galloanserae)**   
	- `python /Code/check_outgroup_mono.py 'PATH/Jarvis/IntronExon_Totalpolytomy/outgroup4.tre' . > ../assess_outgroup4.out`
	- `python /Code/check_outgroup_mono.py 'PATH/Jarvis/IntronExon_Totalpolytomy/outgroup9.tre' . > ../assess_outgroup9.out`
	- check `.out` file, put tree names with count = 0 together in `~/Desktop/test.out` file
	- `python outgroup_filter_move.py combined_assess_file inDIR_with_trees PATH/outgroup_wrong`.
	 
	**Note: the previous version of bp2/bp cannot identify all the genes with misplaced outgroups, and it also misses some genes with correct outgroups. I used another script `compareMLgenes2consedges_Final.py` to check out non monophyletic outgroups, and latter complement this process** (see steps 6 and 7)
	
	- examine branch length
		`python PATH/brlen_dis.py . .contree 1 10 tip/inter`
		figtree check and move these trees to `outgroup_wrong/gallus_weird`  
	- move filtered fasta files
		`python PATH/move_file.py inDIR outDIR .contree`   
	**For UCE**  
	- `python /Code/check_outgroup_mono.py 'PATH/Jarvis/uce/outgroup.tre' . > ../uce_assess_outgroup.out`
	- keep the trees with count = 0
	- filter out long_branches, inter or external branches > 1.0 in Ingroups (not the 4 distant outgroups)
		* `python PATH/brlen_dis.py . .contree 1 10 tip/inter`

6. **Use the following script to check ML tree focal edge relationships**
	* **Exon and Intron reroot**: root the ML tree first using ranking outgroup species
		* `for i in *.treefile; do pxrr -t $i -g Homo_sapiens,Chelonia_mydas,Anolis_carolinensis,Alligator_mississippiensis,Struthio_camelus,Tinamou_guttatus,Anas_platyrhynchos,Gallus_gallus,Meleagris_gallopavo -r -o ../roottree/$i.rr;done`
	* `python ../../Code/compareMLgenes2consedges_Final.py Constree4MLtreecompare  roottree/ 50 No`
	* Change the name of the results based on datatype and edges (1: Basal, 2: Hoazin, 3: Owls).
	  
	* **UCE reroot**:
		* `for i in *.treefile; do pxrr -t $i -g Alligator_mississippiensis,Struthio_camelus,tinamus_major,Anas_platyrhynchos_domestica,Gallus_gallus,Meleagris_gallopavo -r -o ../roottree/$i.rr;done`
	* `python ../../Code/compareMLgenes2consedges_Final.py Constree4MLtreecompare  roottree/ 50 Yes`
	
7. there are some wrong outgroups showed up for each data type and I went back to check the previous filtered-out genes and see if there are some correct outgroups should be retieved. It ended up 4 introns, 476 exons and 2 UCEs that previously filtered out should be back into analyses. In the end we have:
	* 5062 exons (1300 complete matrix)
	* 3370 UCEs (3370 complete matrix)
	* 1409 introns (185 complete matrix)
	
8. repeat step 6 using genes with correct outgroups, summarize bipartition pie chart
	* `mkdir MLtop_distribution`
	* `cd MLtop_distribution/`
	* `python ~/Documents/ML_birdtest/Jarvis/Code/compareMLgenes2consedges_Final.py Constree4MLtreecompare roottree/ 50 No`

9. **Investigate Constraint Edges** 
	- edit `edge_investigate_conflicts_given.py`  
		1. model change to DNA model
		2. use iqtree (**raxml** could be faster and for Prum data I used **raxml**)
	- make the constraint edge unrooted (e.g., only one bipartition)
	- mkdir outDIR
	- `python PATH/edge_investigate_conflicts_given.py edge.tree outname fasta_folder outDIR`
	- `python .../ML_birds/ML_test/JarvisData/Code/edge_investigate_conflicts_given.py '.../ML_test/ConstraintHypothesis/IntronExon_Totalpolytomy/Basal_neoave.tre' Basal_intron.out filter_fasta/ Basal_invest_out/`
	- run this analyses for exon, intron, and UCE for the three testing edges (Basal, Owl, and Hoazin)
	- **run extra analyses for Basal constraints to test single species effect**
		- still conduct the similar iqtree run `edge_investigate_conflicts_given.py` for additional analyses
			* `python ~/Documents/ML_test/Code/edge_investigate_conflicts_given.py Control_edge_assess/contraints_control/Basal_neoave_newcontrol_IE.tre Basal_Cexon4.out fasta4/ Basal_control_exon/`
		- rename the output files "cons_0.iqtree" to "cons_0_conf_4.iqtree"
		- mmv "conf_0/1/2/3.iqtree" to "conf_5/6/7/8.iqtree"
		- combine the results with previous results for treefile and iqtree
		- **Note**: to run the `edge_investigate_conflicts_give.py`, the constraint tree file need to have at least two hypotheses and no blank lines.

10. **summerize Ln*L* scores from constraint and unconstraint iqtree analyses**  
	- exon data
		- move the exon ML iqtrees from exon analyses into `PATH/edge_assess/exon/Basal/` directory
		- run `cd ~/Documents/ML_birds/ML_test/JarvisData/edge_assess/exon/Basal`
		- `python .../ML_birds/ML_test/JarvisData/Code/postprocess_edge_investigate_iqtree.py Basalexon.out .`  
	- uce data
		- cp 3394 UCE unconstaint iqtree into working directory (i.e., `ML_test/JarvisData/edge_assess/UCE/Basal/`)
		- `mmv "*.uce.iqtree" "#1.fasta.iqtree"`
		- `python .../ML_birds/ML_test/JarvisData/Code/postprocess_edge_investigate_iqtree.py Basaluce.out .`  
		- `mv cons_0.csv BasalUCE.csv`
	- intron data
		- cp 1439 intron unconstaint iqtree into working directory (ML\_test/JarvisData/edge\_assess/Intron/Basal/)
		- `python .../ML_birds/ML_test/JarvisData/Code/postprocess_edge_investigate_iqtree.py Basal_intron.out .`  
		- `mv cons_0.csv BasalIntron.csv`
	- I latter noticed the `postprocess_edge_investigate_iqtree.py` has bugs, cannot find the second best hypo, so I modified it to find the second best. Besides, For the output I already have, I wrote another script 	`getbestcons_fromhypo.py` to reorganize results
		- cp the previous csv file columns before `bestone` into a txt file
		- run `python ~/Documents/ML_birdtest/Jarvis/Code/getbestcons_fromhypo.py Hoazinexon.txt best.out`
		- cp the results into the csv and save
		- repeat the above process for all other resulted csv. 


11. manually check the table contents and compare among hypothese.
	- **Cannot finish selection analyses because there are so many pre stop codons**


12. run the script can compare the biparitions between constraints and the best ML tree, this will show whether the best ML gene tree has compatible constraints with the constraint trees and which ones are compatible. 
	* `python ../../Code/compare_bipartition_between_ML_and_constraint_compatible.py ../../../ConstraintHypothesis/IntronExon_Totalpolytomy/Owl.tre treefile/ Exon_Owl_ML.out .treefile`
	* run these analyses for Basal/Hoazin/Owl for each kind of dataset
	* e.g., `python ../../Code/compare_bipartition_between_ML_and_constraint_compatible.py ../../../ConstraintHypothesis/IntronExon_Totalpolytomy/Basal_neoave.tre treefile/ Intron_Basal_ML.out .treefile`
	* the resulting .out files are *_cons.out
	
13. run the script to compare the bipartition conflicts between the best constraint tree and the ML tree, between the best constraint tree and the species tree (from TENT.ExaML.tre), between the ML gene tree and the species tree. This will generate the total number of bipartitions in the best constraint tree, the ML tree and the number of conflict bipartitions among the comparisions.
	* order of resulting column for conflicting bipartitions (bestcons-geneML, bestcons-species, ML-species)
	* `python ../../Code/compare_bipartition_among_besthypo_ML_species.py Basalexon.csv Basal/ treefile/ TENT.ExaML.tre Basalexon_ML_best_species.out`
	* run these analyses for Basal/Hoazin/Owl for each kind of dataset
	* the resulting .out files are *\_ML\_best\_species.out

14. put `conf_best_ML` (output from step 13) and `samebp` (output from step 12) in the output .csv file, use post_organize.py to summarize results
	* `python ~/Documents/ML_test/Code/post_organize.py Basalhypo10_UCE.csv organize.org 3`
	* for loop in shell run several files together
		- `for i in *.csv; do python ~/Documents/ML_birdtest/Jarvis/Code/post_organize.py $i neworganize_UCE/$i.org 1; done`. 	
15. calculate sum of different ln*L* between best and conflict second best, double check if the results are the same as what we got from last step

	``` 
	python Jarvis/Code/summarize_difflnL_between_best_and_conf_secbest.py ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Constraints/IntronExon_Totalpolytomy/Basal_neoave_newcontrol_IE.tre Basalhypo10_intron.csv BasalIntronConflictdifflnL.out
	```

16. calculate the sum of Ng 
	* `for file in *_conf_difflnL.csv; do ID=$(echo $file | awk -F "_" '{print $1}'); python ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Code/organize_sumNg.py $file $ID.sumNg.out; done`

	
17. (add) made ML vs. best figure (build samebp matrix, no support cutoff)
	* `python ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Code/mltopology_distribution.py Basal5exon.csv Basal5.mldis.csv`
	* use `MLvsBest_basal5.r` to make figures
	
## filter genes based on whether ML gene tree compatible with testing constraints. 

`Jarvis data, using 95% support cutoff (bb in IQtree), while Prum's data using 70% support cutoff` (conversation with Nat)

1. `python Prum_Code/ML_compatible_constraint_withcutoff_Prum.py Constraint_treePrum/Basal.tre ML_topology_dist/reroot_tree/ BasalsameMLbp.txt .rr 70`
2. `python Jarvis/Code/ML_compatible_constraint_withcutoff.py constraint.tre MLgenetreefile outname tree_end 95` 

3. calculate sum of different lnL between best and conflict second best, double check if the results are the same as what we got from last step

	``` 
	python Jarvis/Code/summarize_difflnL_between_best_and_conf_secbest.py ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Constraints/IntronExon_Totalpolytomy/Basal_neoave_newcontrol_IE.tre Basalhypo10_intron.csv BasalIntronConflictdifflnL.out edge excludefile (BasalsameMLbp.txt)
	```

4. calculate the sum of Ng 
	* `for file in *_conf_difflnL.csv; do ID=$(echo $file | awk -F "_" '{print $1}'); python ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Code/organize_sumNg.py $file $ID.sumNg.out; done`


## Calculate RF distance among ML gene trees and with the species trees

1. conduct bp analyses for RF distance
	* concatenate ML gene trees and TENT species tree for each data type
	* `bp -rfp -w 100 -t treefile > outfile`
	
2. estimate parameters that will be used in the simulation analyses. 
	- estimate GC variation for each genes and each species across genes
		- `python calculate_deltaGC_spGC.py fasDIR Intron_spGC.out Intron_geneGC.out file_end`
		- e.g., `python ~/Documents/ML_birdtest/Jarvis/Code/calculate_deltaGC_spGC.py retrieve_fasta/ IntronspGC.out IntronGeneGC.out .intron`
	- estimate branch length for ML treefiles for each data type
		- `python /Users/brlab/Documents/ML_birdtest/Jarvis/Code/tipbranch_estimate.py trimmedtre/ .treefile UCE_tipbrlen.txt`
	- organize genes based on GC content variation (low quartile, hight quartile) or tree length, then organize their support for each constraint hypothese separately.  
		* Example runs
		* `python ~/Documents/ML_birdtest/Jarvis/Code/filter_results.py GCbrlen/LowGC.txt . GCbrlen/ Yes LowGC`
		* `python ~/Documents/ML_birdtest/Jarvis/Code/filter_results.py GCbrlen/HighGC.txt . GCbrlen/ Yes HighGC`
		* `python ~/Documents/ML_birdtest/Jarvis/Code/filter_results.py GCbrlen/LowAVEtrelen.txt . GCbrlen/ Yes Lowtrelen`
		* `python ~/Documents/ML_birdtest/Jarvis/Code/filter_results.py GCbrlen/highAVEtrelen.txt . GCbrlen/ Yes Hightrelen`
		* `cd /Users/brlab/Desktop/All_new/Introns/GCbrlen`
		* `for i in *.csv; do python ~/Documents/ML_birdtest/Jarvis/Code/post_organize.py $i $i.org 1; done`
	 

```
Note: Given a same constraint, the same dataset can result in different Ln*L* in repeat runs, and the difference of ln*L* can be larger than 3

```
## Coalescent simulation analyses 

1. simulate gene trees and rescale branch lengths
	* **Use TENT.MP-EST.binned.tre as species tree**
	* reroot and rescale species tree brlen to make it ultramatric
	* e.g., `pxrr -t UCE.TENT.MP-EST.binned.tre -g Struthio_camelus,tinamus_major -o rooted_UCE.TENT.MP-EST.binned.tre`
	* e.g., `python ~/Documents/ML_birdtest/Jarvis/Code/rescale_tree_height.py rooted_TENT.MP-EST.binned.tre rescale_TENT.MP-EST.binned.tre`
	* simulate 500 gene trees and rescale gene tree blen
	* **the number of gene trees should be large enough to recover the species tree using Astral and 500 might be a reasonable number for following analyses**
	* rescaled the internal branches (hereafter inbrlen) of these gene trees with 0.01
	* The tip branch lengths (hereafter tipbrlen) were rescaled based on the tipbrlen distribution of the empirical data of Jarvis et al. (2014). The same random selected percentile was used to generate tipbrlen from the empirical distribution for each species in each individual gene, which ensures a similar tipbrlen pattern across all gene trees as that in the species tree. 
		* `python ~/Documents/ML_birdtest/Jarvis/Code/simulatetrees_and_rescaleBrlen.py ../rescale_TENT.MP-EST.binned.tre 500 ../ExonspTiplen Exonscale0.01 2 0.01`

	 
2. Generate parameter distribution from real dataset and Sequence generation. 
	* Parameter Distribution
		- **base frequency were estimated for each data type based on the filtered Jarvis data**
			1. `python calculate_basefreq.py filter_exon_fasta/ exonGC.txt file_end`
			2. use `GC_distribution.r` to generate histogram distribution for GC and AT for each datatype
			3. for each base content list, use numpy `np.mean(np.log(base))` and `np.std(np.log(base))` to generate sigma and mu for lognormal distribution data
			4. use `np.mean(base)` and `np.std(base)` to generate mean and std for normal distribution data
			5. check out if the real data density and the simulated density based on the estimated mean/std or mu/sigma are matching perfectly.
			
		- alpha and GTR rate collected from Jarvis data
			1. `python calculate_parameter_from_raxml.py fasDIR file_end datatype`
			2. the `RAxml_info.*` files contain the alpha and general rate information for each gene
			3. `grep 'ac ag at cg ct gt' RAxML_info* > alphaGTR_exon.txt`, **`grep 'Base frequencies' RAxML_info* > Basefreq_exon.txt`**
			4. organize the data in excel, output .csv file with header = TRUE
			5. **There are some wrong GTR rate data in UCE and exons, remove them before organize data.**
			6. use `alphaGTRrate.r` to generate distribution plot for each parameter
			7. `python calculate_meanstd_for_alphaGTRbase.py WORKDIR .csv outname` (outname is Sump)
			8. this generate `Sump_exon, Sump_intron, and Sump_UCE`

	* Sequence Generation
		- `python PATH/Generate_sequences.py DIR sump outname`(analyses done in 91)

3. use these generate data, conduct "optimal edge" analyses
	* rename the generated sequences' taxa name
		- `python change_name_for_seqgen.py inDIR outDIR`
	* run `python assessLLS_for_simulation_data.py fasDIR Testing_constraints.tre` on servers 143, 49, and 91.
	* there are 184 testing constraints
	* put all results in `rename_Genmatrix` folder, conduct the consensus tree method1 analyses
		- `cat set*/LLSscore_simdata.out > LLSscore_simdata.all.out`
		- open LLSscore_simdata.all.out in excel and sum all column LLS for each constraint tree
		- put the sum row in a separate text file, called `sumLLS.out`
		- `python compare_constraintLLS_and_generate_contre.py all_unique_constraints.tre sumLLS.out simulation_consensus.tre > adding_ordered.trees`
	* using the additional three methods to build consensus tree
		- modify the `optimal_edge_three_additional_methods.py`, comment or uncomment `outf` variable. 
		- `optimal_edge_three_additional_methods.py testing_constraints diffscorecutoff speciestree LLSscore_list focaledge.out method(3,4,5)`
		- using the generate NewLLS (which filtered out insignificant genes), following method1 to generate sumNewLLS, then make a consensus tree as method2. 
	* methods 1, 2, 3, 4 are presented in the paper.
	* the output files "edgeN.12rank.out" output the best and second best hypotheses for each bipartition edge in the species tree (e.g., TENT_MPEST/TNET_ExaML), as well as their LnL difference. 
	* the focal edges and number can be found in `focaledge.out` (e.g., `SeqGen1_focaledge_withconf_constraints.out`) file.
	
4. use Astral to build species from simulated gene trees or generated RAxML trees
	* `astral -i simtrees -r 200 -o astral_from_simtrees.tre`
	* `astral -i RAxML_MLtrees_forseqGen.tre -r 200 -o astral_from_MLtrees.tre`

5. conduct concatenated analyses based on simulated dataset, for latter RF distance comparison
	* pxcat -s *.fasta -o Exon/Intron/UCE.cat.fa -p partition
	* use `raxml-ng --all --msa SeqGen1_exon.cat.fa --model SeqGen1_exon.part.txt --tree pars{5} --bs-trees 200 --threads 8` to conduct ML analyses. 

6. find bipartition constraint order in the Testing_constraint.tre
	* `python ~/Documents/ML_birdtest/Jarvis/Code/find_tree_order.py Testing_constraints.tre`

7. **check MLgene tree distribution** 
	* `python ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Code/split_MLgene_file.py Seqgen2.bestMLtree.tre Seqgen2MLtrees`
	* `for i in *.tre; do pxrr -t $i -g Struthio_camelus Tinamou_guttatus -o roottree/$i.rr;done`
	* `python ~/Documents/Ongoing_project/ML_birdtest/Jarvis/Code/compareMLgenes2consedges_simulation.py ../../Constree4MLtreecompare Seqgen1MLtrees/roottree/ 0` (uninfomration < 2)
	* use ML_topdistribution.r build a figure about how the simulated gene trees distributed as to the focal edges.

8. **check simulated True gene tree distribution**
	* `replace '[&R]' and "\_1" with nothing in simtrees\_1 and simtrees\_2
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/avian_conflict_assess/src/split_MLgene_file.py simtrees_1 ExonSimTrueTrees1`
	* `mkdir rootSimtree1/`
	* `for i in *.tre; do pxrr -t $i -g Struthio_camelus Tinamou_guttatus -o rootSimtree1/$i.rr;done`
	* `for i in *.tre; do pxrr -t $i -g Struthio_camelus Tinamou_guttatus -o rootSimtree2/$i.rr;done`
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/compareMLgenes2consedges_simulation.py Constree4MLtreecompare rootSimtree1/ 0`
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/compareMLgenes2consedges_simulation.py Constree4MLtreecompare rootSimtree2/ 0`

9. check how similarity between the simulated species tree and the true species tree. Using RF distance:
	* put 18 species tree (12 coalescent tree and 6 concatenated tree) as well as the TENT-MPest tree in a same file
	* then run `bp -rfp -w 100 -t treefile > outfile`
	* if the RF distance is 8 between the reconstructed species tree and the true species tree, I use 8 divided by 94 (47 internal nodes) and get 0.085 difference of relationships

10. estimate the GTEE (gene tree esitmation error) using unweighted RF distance between true gene tree and estimated ML gene tree
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/avian_conflict_assess/src/RF_distance_dendropy.py simtrees_2 MLtree_distribution/IntronSeqgen2_MLgenetrees/oriMLgenetrees/ Intron_sim2_ML2_RF.dist`

11. estimate ILS using unweighted RF distances between true species tree (TENT_MP_est) and true gene tree (e.g.)
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/avian_conflict_assess/src/RF_dist_speciestree2genetree.py UCEscale0.01_2 ../TENT.MP-EST.binned.tre UCE_sim2_TENT.RF.dist`
	* `python ~/Documents/Ongoing_project/ML_birdtest/Bitbucket/avian_conflict_assess/src/RF_dist_speciestree2genetree.py Exonscale0.01_1 ../TENT.MP-EST.binned.tre Exon_sim1_TENT.RF.dist`

	
************

	
## Prum dataset  

Prum et al. (2015) used genomic sequence data from each of 198 species of living birds, representing all major avian lineages. They followed anchored hybrid enrichment, yielding 259 nuclear loci with an average length of 1,523 bases for a total data set of over 7.8 × 107 bases.
Based on above results from Jarvis data, we tested the following hypothese using Prum data, since RAxML is faster than IQ-TREE in estimating likelihood scores, we used the RAxML in our analyses (i.e., chenge raxml=True in edge_investigate_conflicts_given.py).

1. Constraints:
	* Basal
		- A-**V**
		- A-**VII**
		- A-Doves
	* Hoazin
		- *H*-**I**
		- *H*-*S*
		- *H*-**V**
	* Owl
		- Owl-*M+W*
		- Owl-*M*
		- Owl-*EHV*
2. Conduct edge analyses using the Trimmed_alignments (259 loci)
	* put all loci in PATH/Edge_assess/fasta folder
	* put above constraint trees in Constraint/ folder
		- **note** delete empty lines in the constraint tree file, otherwise there will be error running the analyses
	* run `python edge_investigate_conflicts_given2.py Constraint/Basal.tre BasalPrum.out fasta/ BasalPrum/`
	* run the same analyses for Hoazin.tre and Owl.tre
3. Calculate the likelihood scores from the ML tree for each gene (the RAxML_tree from Prum et al. data)
	* the working dir for this analyses is: `ningwum@141.211.236.91:~/Documents/ML_birds/ML_test/Holocentrus-Prum_et_al_2015-a03a2b5/Trees/RAxML-GeneTrees`
	* the fasta file from Prum and the gene tree has different taxa names, so first change tree taxa name consistent with fasta file using:
		- `python ../../Prum_Code/gene_tree_taxaname_match.py RAxMLconcatenate.tre RAxML_bipartitions/ RAxML_genetree_match/`
	* then conduct the calculation of LLS for individual genes using:
		- `python ../../Prum_Code/calculate_geneLL_raxml.py RAxML_genetree_match/ fasta/ OrganizeML.out`
4. Post analyses from edge_conflict run
	* use `postprocess_edge_investigate.py` to organized LLS from all constraint trees (with RAxML)
	* cd Edge_assess/Basal, run `python postprocess_edge_investigate.py BasalPrum.out BasalPrum/`
	* mv con0.csv BasalPrum.csv
	* same analyses for Hoazin/ and Owl/
	* copy those *.csv files into Dropbox/ML_test/Prum/ folder

5. test selection for L111.fasta outlying genes
	* match fasta and tree file seq name 
		- grep ">" L111.fasta > taxa.txt
		- using the following python to extract tree leaves name
		- put into excel, exclude duplication, find unmatched seq name
		- change tree leave name (same to fasta)
		- delete the first 2 and last 4 character, including stop codon. 
		- run HYPHY (aBSREL)

6. ML topology distribution (note use those tree name match the fasta files)
	* `for i in RAxML_bipartitions.*; do pxrr -t $i -g I0461_HERP_15451_Alligatoridae_Caiman_croccodilus,I0460_HERP_14722_Crocodylidae_Crocodylus_porosus -r -o ../reroot_tree/$i.rr;done`
	* `python ../Prum_analyses/Prum_Code/compareMLgenes2consedges_Final_Prum.py threeCons_edges reroot_tree/ 0` 
	* **no support cutoff, and as long as the relationship show in more than two genes, they will show in the pie chart**
	* `python ../../Prum_analyses/Prum_Code/compareMLgenes2consedges_Final_Prum.py threeCons_edges reroot_tree/ 50`. **used for comparison**
