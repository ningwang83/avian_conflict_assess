#! /usr/bin/env python


import os,sys
import tree_reader
from biparts import Bipart,get_biparts
from node import Node
import numpy as np
import csv


if len(sys.argv) != 6:
	print "python "+sys.argv[0]+" consfile csvfile outname edge excludefile"
	sys.exit(0)

consfile,csvfile,outname,edge,excfile = sys.argv[1:]
curDIR = os.getcwd()+"/"

Exclude = []
# read excludefile, exclude genes cannot be compatible with any constraint
with open(excfile,"rU") as exc:
	for geneli in exc:
		locus = geneli.strip().split("\t")[0]
		keep = geneli.strip().split("\t")[1]
		if keep  == "NO":
			Exclude.append(locus)

# build dictionary about corresponding bp for each constraint
consdic = {}
First = True
count = 0
with open(consfile,"rU") as handle:
	for line in handle:
		if First == True:
			root = tree_reader.read_tree_string(line.strip())
			bp = get_biparts(root)[0]
			consname = "cons_0"
			# print consname
			consdic[bp] = consname
			First = False
		else:
			root = tree_reader.read_tree_string(line.strip()[1:])
			bp = get_biparts(root)[0]
			consname = "cons_0_conf_"+str(count)
			# print consname
			consdic[bp] = consname
			count += 1
# setup conflict hypos for each constraint
cons_conflict = {}
for key in consdic.keys():
	maincons = consdic[key]
	cons_conflict[maincons] = []
	for k in consdic.keys():
		if k.conflict(key):
			cons_conflict[maincons].append(consdic[k])

# read csv and calculate sum of diff lnL between best and conflict second best 
sumall = {}
sumsig = {} # diff lnL > 2

infile = open(csvfile,"rU")

reader = csv.reader(infile)
header = next(reader)

title = {}
for index,column in enumerate(header):
	title[column] = index

diffout = open(curDIR+edge+"_conf_difflnL.csv","w")
diffout.write("gene,bestone,confdifflnL\n")
for row in reader:
	if "-" in row: continue # delete gene with missing constraint
	genename = row[0]
	genelocus = genename.split(".")[0]
	if genelocus in Exclude: continue # delete genes not compatible with any constraints
	besthypo = row[title["bestone"]]
	bestscore = float(row[title[besthypo]])
	conflict_scores = []
	for confcons in cons_conflict[besthypo]:
		hyposcore = float(row[title[confcons]])
		conflict_scores.append(hyposcore)
	if len(conflict_scores) == 0: continue # BDEG has no conflict with the first five hypos
	secbest = sorted(conflict_scores)[-1] 
	difflnL = bestscore - secbest
	diffout.write(genename+","+besthypo+","+str(difflnL)+"\n")
	if not besthypo in sumall.keys():
		sumall[besthypo] = difflnL
	else:
		sumall[besthypo] += difflnL
	if difflnL > 2:
		if not besthypo in sumsig.keys():
			sumsig[besthypo] = difflnL
		else:
			sumsig[besthypo] += difflnL

infile.close()

# write output file
with open(curDIR+outname,"w") as outfile:
	outfile.write("constraints\tallsumdifflnL\tsigsumdifflnL\n")
	for key in sorted(sumsig.keys()):
		outfile.write(key+"\t"+str(sumall[key])+"\t"+str(sumsig[key])+"\n")
		


		

			