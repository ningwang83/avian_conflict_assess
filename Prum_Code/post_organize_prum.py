#! /usr/bin/env python

"""use this script to summarize the focal edge test result
the column being used are 'gene', 'bestone' 'diffbestsecondbest' 'conf_best_ML', 'samebp'
the number of constraints are different so the column number are different

"""


import os,sys
import numpy as np
import csv

def top_supporter(dic,number): # dic is the gene:diffscore dictionary, number is the number of top supported genes
	diffscores = sorted(dic.values())
	topscores = diffscores[-number:]
	topgene = []
	for k, v in dic.items():
		if v in topscores:
			topgene.append(k)
	return topgene
		

if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" csvfile outname topgene_number"
	print "the topgene_number is the number of top supported genes you want to see, e.g., 3"
	sys.exit(0)

csvfile,outname,number = sys.argv[1:]
curDIR = os.getcwd()+"/"
number = int(number)

infile = open(curDIR+csvfile,"rU")

reader = csv.reader(infile)
header = next(reader)

title = {}
for index,column in enumerate(header):
	title[column] = index

bestcons_nonsig = {} # the number of genes support one constraint, a dic of constraint:{gene:difscore}
#confbp_dic = {} # the total number of conflict bipartitions between best constraint vs ML, averaged latter
consLLS = []
for i in range(1,title["bestone"]):
	consLLS.append(0)
#bplist = []

First = True
for row in reader:
	if "-" in row: continue # delete gene with missing constraint
	for c in range(1,title["bestone"]):
		consLLS[c-1] += float(row[c])  
	genename = row[title["gene"]]
	bestone = row[title["bestone"]]
	difscore = row[title["diffbestsecondbest"]]
	#confbp = row[title["conf_best_ML"]] # conflict bipartitions between the best constraint and ML gene tree
	#samebp = row[title["samebp"]]
	#if samebp != "NO":
	#	bplist.append(int(samebp))
	if First:
		bestcons_nonsig[bestone] = {}
		bestcons_nonsig[bestone][genename] = float(difscore)
		#confbp_dic[bestone] = int(confbp)
		First = False
	else: 
		if not bestone in bestcons_nonsig.keys():
			bestcons_nonsig[bestone] = {}
			bestcons_nonsig[bestone][genename] = float(difscore)
			#confbp_dic[bestone] = int(confbp)
		else:
			bestcons_nonsig[bestone][genename] = float(difscore)
			#confbp_dic[bestone] += int(confbp)

infile.close()
outfile = open(curDIR+outname,"w")
outfile.write("cons\tTotalLLS\tSupportNum\tSigsupportNum\tTotaloutlier\tTopgenes\n")
cnum = 0
for cons in sorted(bestcons_nonsig.keys()):
	gene_list = bestcons_nonsig[cons].keys()
	difscore_list = sorted(bestcons_nonsig[cons].values())
	sigdifscore = [i for i in difscore_list if i > 2]
	#median = np.median(sigdifscore)
	#Q1 = np.percentile(sigdifscore, 25)
	#Q3 = np.percentile(sigdifscore, 75)
	median = np.median(difscore_list)
	Q1 = np.percentile(difscore_list, 25)
	Q3 = np.percentile(difscore_list, 75)
	IQR = Q3-Q1
	minlimit = Q1 - 1.5*IQR
	maxlimit = Q3 + 1.5*IQR
	outlier = []
	for i in gene_list:
		if bestcons_nonsig[cons][i] > maxlimit: # or bestcons_nonsig[cons][i] < minlimit:
			outlier.append(i)
	#print len(outlier)
	topgenes = top_supporter(bestcons_nonsig[cons],number)
	Numsupport = len(gene_list)
	Numsigsupport = len(sigdifscore)
	#Aveconfbps = confbp_dic[cons]/Numsupport
	#print bplist.count(cnum)
	outfile.write(cons+"\t"+str(consLLS[cnum])+"\t"+str(Numsupport)+"\t"+str(Numsigsupport)+"\t"+str(len(outlier))+"\t"+",".join(topgenes)+"\n")
	cnum += 1