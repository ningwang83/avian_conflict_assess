#! /usr/bin/env python

import os,sys
import tree_reader

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" treeDIR fastaDIR organizefilename"
		sys.exit()
	treeDIR,fastaDIR,outname = sys.argv[1:]
	treeDIR = os.path.abspath(treeDIR)+"/"
	fastaDIR = os.path.abspath(fastaDIR)+"/"
	curDIR = os.getcwd()+"/"
	
	# run raxml to calculate gene-wise LLS, RAxML_log file has LLS as the second collumn
	for i in os.listdir(treeDIR):
		locus = i.split(".")[-1]
		fastafile = fastaDIR+locus+".fasta"
		raxmlcmd = "raxmlHPC-PTHREADS-AVX -f e -s "+fastafile+" -t "+treeDIR+i+" -m GTRGAMMA -n "+locus
		os.system(raxmlcmd)
		try:
			os.remove("RAxML_result."+locus)
			os.remove("RAxML_info."+locus)
			os.remove("RAxML_binaryModelParameters."+locus)
		except: pass
		
	# organize LLS from RAxML_log file, put into a Dictionary
	LLSDic = {}
	for log in os.listdir(curDIR):
		if not log.startswith("RAxML_log"): continue
		locusname = log.split(".")[-1]
		with open(curDIR+log,"rU") as handle:
			line = handle.readline().strip()
			LLS = line.split(" ")[-1]
			LLSDic[locusname] = LLS
	
	# open outfile and write results
	outfile = open(curDIR+outname,"w")
	for key in sorted(LLSDic.keys()):
		outfile.write(key+"\t"+LLSDic[key]+"\n")
	outfile.close()
		
		

