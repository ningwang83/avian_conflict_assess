#! /usr/bin/env python

from biparts import Bipart,get_biparts
import os,sys
import tree_reader

def get_biparts_cutoff(rt,supportcut):
    bps = []
    out = set(rt.lvsnms())
    for i in rt.iternodes():
        if len(i.children) == 0:
            continue
        if i == rt:
            continue
        if i.length < 0.0000:
            continue
        if i.label == "" or int(i.label) < supportcut:
        	continue
        right = (set(i.lvsnms()))
        left = (set(out-right))
        bp = Bipart(left,right)
        bps.append(bp)
    return bps



if len(sys.argv) != 6:
	print "python "+sys.argv[0]+" constraint.tre MLtreeDIR outfile tree_end supportcutoff"
	sys.exit()

constraint,MLtreeDIR,outname,tree_end,cutoff = sys.argv[1:]
MLtreeDIR = os.path.abspath(MLtreeDIR)+"/"
curDIR = os.getcwd()+"/"

# read constraint tree file, put into tree string list
constraint_tree_list = []
with open(constraint,"rU") as infile:
	for li in infile:
		if li.startswith("-"):
			tree_string = li.strip()[1:]
		else:
			tree_string = li.strip()
		constraint_tree_list.append(tree_string)

# read MLtree from DIR and compare MLtree bipartitions with constraint_tree bp
Dict = {}
for ML in os.listdir(MLtreeDIR):
	if not ML.endswith(tree_end): continue
	treename = ML.split(".")[1]
	Dict[treename] = []
	intree = open(MLtreeDIR+ML,"rU")
	treeline = intree.readline().strip()
	MLtree = tree_reader.read_tree_string(treeline)
	MLbps = get_biparts_cutoff(MLtree,int(cutoff))
	intree.close()
	for constre in constraint_tree_list:
		tree_hypo = tree_reader.read_tree_string(constre)
		consbps = get_biparts(tree_hypo)
		consbp = consbps[0]
		count = 0
		if len(MLbps) == 0:
			Dict[treename].append("yes")
			continue
		for MLbp in MLbps:
			if MLbp.conflict(consbp): # should not use equal, see reasons below
				count += 1
				break
		if count > 0:
			Dict[treename].append("no")
		else:
			Dict[treename].append("yes") # ML don't need to be the same, they just compatible with the constraints

# open outfile and write results
outfile = open(curDIR+outname,"w")
for key in Dict.keys():
	if not "yes" in Dict[key]:
		outfile.write(key+"\tNO\n")
	else:
		Compatiblec = []
		for n in range(len(Dict[key])):
			if Dict[key][n] == "yes":
				Compatiblec.append(str(n))
		outfile.write(key+"\t"+"_".join(Compatiblec)+"\n")

outfile.close()

""" some cases only assess equal is not enough for missing taxa
for example constraint1: ABCD|EFGH, constraint2: ABC|DEFGH; one MLtree is ABC|EFG, the other is ABD|EFH, 
these two trees are all equal to constraint tree1 and ABC|EFG also equal to constraint2 if we use bp.equal(inbp)
"""
