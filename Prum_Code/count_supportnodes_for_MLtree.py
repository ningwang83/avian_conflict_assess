#! /usr/bin/env python

# read gene trees and calculate the number of nodes that support (>70%) by each gene
# use the results to make histogram

import tree_reader
import os,sys
from node import Node


if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" intreeDIR outfile supportcut"
	sys.exit(0)

treDIR,outname,spcutoff = sys.argv[1:]
curDIR = os.getcwd()+"/"
treDIR = os.path.abspath(treDIR)+"/"

countDict = {} # file name: support node number
for tre in os.listdir(treDIR):
	if not tre.endswith("rr"): continue
	print tre
	nodenum = 0
	filename = tre.split(".")[1]
	with open(treDIR+tre,"rU") as infile:
		trestr = infile.readline()
		root = tree_reader.read_tree_string(trestr)
	
	for node in root.iternodes():
		if node.istip or node == root: continue
		#print node.label
		if node.label == "": continue
		sup = int(node.label)
		if sup >= int(spcutoff):
			nodenum += 1
	countDict[filename] = str(nodenum)

with open(curDIR+outname,"w") as outfile:
	for key in countDict.keys():
		outfile.write(key+"\t"+countDict[key]+"\n")