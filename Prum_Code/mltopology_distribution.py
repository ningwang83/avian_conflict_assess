#! /usr/bin/env python

"""
find whether the ML tree are consistent with the supported best topology

"""


import os,sys
import numpy as np
import csv

if len(sys.argv) != 3:
	print "python "+sys.argv[0]+" csvfile outname"
	sys.exit(0)

csvfile,outname = sys.argv[1:]
curDIR = os.getcwd()+"/"

infile = open(curDIR+csvfile,"rU")

reader = csv.reader(infile)
header = next(reader)

title = {}
for index,column in enumerate(header):
	title[column] = index # index-1 corresponding to samebp for each constraints
	
# the total number of genes support for each constraints
tnum_support = []
for i in range(1,title["bestone"]):
	tnum_support.append(0)

added = []
MLdistri = {}
for row in reader:
	if "-" in row: continue # delete gene with missing constraint
	bestone = row[title["bestone"]]
	samebp = row[title["samebp"]]
	numindex = title[bestone] - 1
	if not bestone in added:
		added.append(bestone)
		MLdistri[bestone] = []
		for i in range(1,title["bestone"]):
			MLdistri[bestone].append(0)
		MLdistri[bestone].append(0)
	if str(numindex) in samebp:
		tnum_support[numindex] += 1
	if "_" in samebp:
		samebp_list = samebp.split("_")
		length = float(len(samebp_list))
		for bp in samebp_list:
			MLdistri[bestone][int(bp)] += 1/length
	elif samebp == "NO":
		MLdistri[bestone][-1] += 1 # the last one is other topology
	else:
		MLdistri[bestone][int(samebp)] += 1

infile.close()

outfile = open(curDIR+outname,"w")

# the first number is the number of ML tree compatible with bestone
# the rest is the number of ML tree partially or completely contribute to one hypothesis

for key in sorted(MLdistri.keys()):
	outfile.write(key+","+str(tnum_support[title[key]-1])+","+",".join([str(h) for h in MLdistri[key]])+"\n")
	
outfile.close()
		
			