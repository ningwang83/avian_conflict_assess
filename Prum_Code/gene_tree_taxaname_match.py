#! /usr/bin/env python

import os,sys
from node import Node
import tree_reader


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" concatetree treeDIR outtreeDIR"
		sys.exit()
	concatetree,treeDIR,outtreeDIR = sys.argv[1:]
	treeDIR = os.path.abspath(treeDIR)+"/"
	outtreeDIR = os.path.abspath(outtreeDIR)+"/"
	
	# read concatetree and save the taxa name to a Dictionary
	taxaDic = {}
	with open(concatetree,"rU") as infile:
		tree_string = infile.readline().strip()
		tree = tree_reader.read_tree_string(tree_string)
		for i in tree.leaves():
			label = i.label
			number =  label.split("_")[0]
			taxaDic[number] = label
	# read gene tree files and change tip name matching concatenate tree
	for tre in os.listdir(treeDIR):
		if not tre.startswith("RAxML_bipartitions."): continue
		outtree = open(outtreeDIR+tre,"w")
		with open(treeDIR+tre,"rU") as handle:
			line = handle.readline().strip()
			tree = tree_reader.read_tree_string(line)
			for i in tree.leaves():
				num = i.label.split("_")[0]
				i.label = taxaDic[num]
			outtree.write(tree.get_newick_repr(True)+";\n")
		outtree.close()

			
	

