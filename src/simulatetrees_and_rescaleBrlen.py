#! /usr/bin/env python

# from a coalescent species tree simulate 200 gene trees
# 1. Use species tip distribution to generate 5000 tip branch lengths
# 2. sorted the list for each species 
# 3. randomly select a number n from 0-100
# 4. retrieve the n% percentile of the brlen from the sorted list of a species
# try different interscale to make Astral recovered the true tree

import os,sys
import numpy as np
from random import *
import tree_reader
import dendropy# must be Dendropy 4.2.0
from dendropy.simulate import treesim
from dendropy.model import reconcile
from dendropy.interop import seqgen
from dendropy.datamodel.charmatrixmodel import CharacterMatrix
import glob
import subprocess


def generate_tipbrlens(distri_dic):
	# distridic with species as key, and [mean,std] as values
	tiplens_dic = {}
	for key in distri_dic.keys():
		mean = distri_dic[key][0]
		std = distri_dic[key][1]
		tiplens = np.random.normal(mean,std,5000)
		tiplens_dic[key] = sorted(tiplens)
	return tiplens_dic

def percentilelen(random,tiplens_dic):
	percent_tiplen = {}
	for key in tiplens_dic.keys():
		tipls = tiplens_dic[key]
		perlen = np.percentile(tipls, random)
		rp = round(perlen,5)
		percent_tiplen[key] = rp
	return percent_tiplen

	
if len(sys.argv) != 7:
	print "python "+sys.argv[0]+" speciestree numtrees spTiplen.txt outname numrepeat interscale"
	print "outname: scalesimtrees, numrepeat = 2, interscale try 0.01,0.02,0.03"
	sys.exit()

speciestre,numgenes,sptipfile,outtree,numrepeat,interscale = sys.argv[1:]
curDIR = os.getcwd()+"/"
numrepeat = int(numrepeat)

# read sptipfile and read species mean and std for tip length
# [species,medianbr,meanbr,stdbr,max,min]
count = 3
tipbrlenDic = {}
with open(sptipfile,"rU") as handel:
	for l in handel:
		if count > 0:
			count -= 1
			continue
		else:
			line = l.strip().split("\t")
			spname = line[0]
			mean = float(line[2])
			std = float(line[3])
			tipbrlenDic[spname] = [mean,std]
# generate 5000 tip branch data for each species
tiplens_dic = generate_tipbrlens(tipbrlenDic)

# read species tree and simulate gene trees
while numrepeat > 0:
	if not os.path.exists("simtrees_"+str(numrepeat)):
		t = dendropy.Tree.get_from_path(speciestre,'newick')
		#print(t.as_ascii_plot(plot_metric='length'))
		genes_to_species = dendropy.TaxonNamespaceMapping.create_contained_taxon_mapping(
							containing_taxon_namespace=t.taxon_namespace,
							num_contained=1)
		simtrees = open(curDIR+"simtrees_"+str(numrepeat),"w")	
		for j in range(int(numgenes)):
			gene_tree = treesim.contained_coalescent_tree(containing_tree=t,
			gene_to_containing_taxon_map=genes_to_species)
			simtrees.write(gene_tree.as_string(schema='newick'))
		simtrees.close()
	# read simulate trees and rescale the internodes with 0.01 and the tip branches with percentile information
	# simtree file format "[&R] (a,(b,c));"
	simfile = open(curDIR+"simtrees_"+str(numrepeat),"rU")
	outfile = open(curDIR+outtree+"_"+str(numrepeat),"w")
	for simline in simfile:
		treestr = simline.strip().split(" ")[1]
		root = tree_reader.read_tree_string(treestr)
		while True:
			random = randint(1, 5000) / 50.0
			percent_tiplen = percentilelen(random,tiplens_dic)
			count = 0
			for node in root.leaves():
				sp = node.label[:-2]
				node.length = percent_tiplen[sp]
				if node.length < 0:
					node.length = 0
					count += 1
			if count <= 3: # don't want too many tip branch to be 0
				break
		for n in root.iternodes():
			if n == root: continue
			elif n.istip:
				spname = n.label[:-2]
				n.label = spname
			else:
				nlen = n.length * float(interscale)
				n.length = nlen
		outfile.write("[&R] "+root.get_newick_repr(True)+";\n")
	numrepeat -= 1


			
			
	
	