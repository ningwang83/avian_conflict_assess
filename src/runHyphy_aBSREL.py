#! /usr/bin/env python

# use this to run hyphy or exon genes

import os,sys
import tree_reader
from seq import read_fasta_file

# hyphy version  = 2018/08/16 git clone from source
# the hyphy selection changed from previous versions

def run_hyphy(alnfile,treefile,output):
	ctl = treefile+".hyphy.ctl"
	with open(ctl,"w") as outfile:
		outfile.write("1\n6\n1\n")
		outfile.write(alnfile+"\n"+treefile+"\n")
		outfile.write("2\n")
		#outfile.write(output)
	os.system("HYPHYMP < "+ctl+" > "+alnfile+".result")
		

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python "+sys.argv[0]+" inDIR"
		sys.exit(0)
	inDIR = sys.argv[1]
	inDIR = os.path.abspath(inDIR)+"/"
	# run hyphy
	for runf in os.listdir(inDIR):
		if not runf.endswith(".fa"): continue
		clusterID = runf.split(".")[0]
		alnfile = inDIR+runf
		treefile = inDIR+clusterID+".tre.rr"
		output = inDIR+clusterID
		run_hyphy(alnfile,treefile,output)

