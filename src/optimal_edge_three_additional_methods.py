#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts
import glob
from assessLLS_for_each_edge_on_completedata import *
from compare_constraintLLS_and_generate_contre import *

""" three methods for ranking:
3: no focal edge, filter tree based on deltaLn > 2 and then ranking constraints based on sum of diff Ln
4: within focal edge ranking, chosed the best one from each focal edge, build contre
5: within focal edge compare, combine all focal edge results and rank, build contre
The other first two methods are used sum LnL from all genes or significant genes

modify the script as needed, comment outf, line 144-145
""" 

def get_confs(bp,bps): # return bipartitions, also return equal one
	conflicts = []
	for i in bps:
		if i.conflict(bp):
			conflicts.append(i)
		elif i.equal(bp):
			conflicts.append(i)
	return conflicts

def get_bestone_and_diffs(numlist,allcons):
	focalcons = []
	for i in numlist:
		#print i
		score = float(allcons[i])
		focalcons.append(score)
	ordercons = sorted(focalcons)
	#print ordercons
	diff = ordercons[-1] - ordercons[-2]
	if diff < 2:
		diff = 0 # not contribute for final score sumup
	ind = focalcons.index(ordercons[-1])
	cnum = numlist[ind]
	return cnum,diff

def getbp(contre):
	root = tree_reader.read_tree_string(contre)
	bplist = get_biparts(root)
	conbp = bplist[0]
	return conbp
	
def merge_two_dicts(x, y):
	for ki in y.keys():
		if not ki in x.keys():
			x[ki] = y[ki]
		elif y[ki] > x[ki]:
			x[ki] = y[ki]
		else:
			continue
	return x

if __name__ == "__main__":
	if len(sys.argv) != 7:
		print "python "+sys.argv[0]+" testing_constraints diffscorecutoff speciestree LLSscore_list focaledge_out method(3,4,5)"
		sys.exit(0)
	consfile,cutoff,speciestre,scorefile,outname,method = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	cutoff = float(cutoff)
	#generate testing bps from testing_constraints
	testbps = {}
	testcons = {}
	with open(consfile,"rU") as handle:
		for l in handle:
			connum = l.strip().split("\t")[0]
			constre = l.strip().split("\t")[1]
			testcons[int(connum)] = constre
			root = tree_reader.read_tree_string(constre)
			bplist = get_biparts(root)
			consbp = bplist[0]
			testbps[consbp] = int(connum)
	
	# read species tree and build 48 focal edges with conflict constraints
	speciestre_bps = generate_bp(speciestre)
	allbps = testbps.keys()
	focaledges = {} ##
	count = 0
	outf = open(curDIR+outname,"w")
	universal_support_bp = []
	for bp in speciestre_bps:
		focalcon = get_cons_from_bp(bp)
		confbps = get_confs(bp,allbps)
		focedge_num = []
		for b in confbps:
			focedge_num.append(testbps[b])
		if len(focedge_num) == 1: # this bipartition has no conflict and should be added to the species tree
			universal_support_bp.append(bp)
		elif len(focedge_num) > 1: # there is a bipartition has all species
			focaledges["edge"+str(count)] = focedge_num
			outf.write(str(count)+"\t"+focalcon+"\t".join([str(n) for n in focedge_num])+"\n")
		count += 1
	outf.close()
	
	# read LLSscore_list and setup a dic
	LLSscores = {}
	bestMLscore = {}
	if not scorefile.endswith(".csv"):
		First = True
		with open(scorefile,"rU") as infile:
			for li in infile:
				if First:
					First = False
					continue
				genename = li.strip().split("\t")[0]
				consLLS = li.strip().split("\t")[1:-1]
				bestML = li.strip().split("\t")[-1]
				LLSS = [float(i) for i in consLLS]
				num = range(184)
				consdic = dict(zip(num,LLSS))
				# print consLLS
				LLSscores[genename] = consdic
				bestMLscore[genename] = bestML
	else:
		with open(scorefile,"rU") as infile:
			for li in infile:
				genename = li.strip().split(",")[0]
				consLLS = li.strip().split(",")[1:-1]
				bestML = li.strip().split(",")[-1]
				LLSS = [float(i) for i in consLLS]
				num = range(184)
				consdic = dict(zip(num,LLSS))
				# print consLLS
				LLSscores[genename] = consdic
				bestMLscore[genename] = bestML
	
	# for each local edge, each support cons, the sum of gene LLS (by only considering genes of delta diff > 2)
	if method == "4" or method == "5":
		rankingcons = {} # method 4
		allranks = {} # method 5, not mentioned in the paper
		for edge in focaledges.keys():
			sumLLS_in_edge = {}
			count_in_edge = {}
			diffLLS_list = {}
			Cnumlist = focaledges[edge]
			for gene in LLSscores.keys():
				# print LLSscores[gene]
				cnum,diff = get_bestone_and_diffs(Cnumlist,LLSscores[gene])
				if not cnum in sumLLS_in_edge.keys():
					sumLLS_in_edge[cnum] = diff
					count_in_edge[cnum] = 1
					diffLLS_list[cnum] = []
					diffLLS_list[cnum].append(str(diff))
				else:
					sumLLS_in_edge[cnum] += diff
					count_in_edge[cnum] += 1
					diffLLS_list[cnum].append(str(diff))
			allranks = merge_two_dicts(allranks,sumLLS_in_edge)
			max = 0
			second = 0
			best = ""
			sec = ""
			sortsumLLS = sorted(sumLLS_in_edge.items(), key=lambda x: x[1], reverse=True)
			best = testcons[sortsumLLS[0][0]]
			max = sortsumLLS[0][1]
			second = testcons[sortsumLLS[1][0]]
			secs = sortsumLLS[1][1]
			with open(curDIR+edge+".12rank.out","w") as hand:
				hand.write(best+"\n"+second+"\n"+str(max-secs)+"\n")
			with open(curDIR+edge+".allranks.out","w") as rankout:
				for consk in sortsumLLS:
					rankout.write(testcons[consk[0]]+";"+str(consk[1])+"\t"+str(count_in_edge[consk[0]])+ \
					"\t"+",".join(diffLLS_list[consk[0]])+"\n")
			if not best in rankingcons.keys():
				rankingcons[best] = max
			else:
				if rankingcons[best] < max:
					rankingcons[best] = max
				else: continue
	else:
		# for each gene, identify the bestone, compare the bestone versus the second conflict bestone, added up
		# diffscore > cutoff, rank the bestones based on sum of diffscore
		bestcons = {} # method 3
		NewLLSfile = open(curDIR+"newLLS.out","w")
		for gene in LLSscores.keys():
			# print gene
			dic = LLSscores[gene]
			sortdic = sorted(dic.items(), key=lambda x: x[1], reverse=True) # the dic becomes a list with sets in it
			sortkey = sorted(dic.items(), key=lambda x: x[0])
			first = sortdic[0]
			bestone = first[0]
			bestcon = testcons[bestone]
			bestbp = getbp(bestcon)
			bestscore = first[1]
			for k in sortdic[1:]:
				com_one = k[0]
				com_con = testcons[com_one]
				com_bp = getbp(com_con)
				# if a bipartition has no conflict in the constraints, it can be treated highly supported 
				# then the diff scores are calculated between the best and the last
				if com_bp.conflict(bestbp) or k == sortdic[-1]: 
					sedconfscore = k[1]
					diffscore = bestscore-sedconfscore
					print gene, diffscore
					if diffscore < cutoff:
						diffscore = 0
					else:
						sortscore = [str(v[1]) for v in sortkey]
						NewLLSfile.write(gene+"\t"+"\t".join(sortscore)+"\t"+bestMLscore[gene]+"\n")
					if  not bestcon in bestcons:
						bestcons[bestcon] = diffscore
					else:
						bestcons[bestcon] += diffscore
					break
		NewLLSfile.close()
	# sorted the Best_trees dictionary based on tree scores and make a consensus tree
	# print bestcons.keys()
	if method == "4":
		Dic_list = sorted(rankingcons.items(), key=lambda x: x[1], reverse=True)
	elif method == "5":
		new_allranks = {}
		for j in allranks.keys():
			new_allranks[testcons[j]] = allranks[j]
		Dic_list = sorted(new_allranks.items(), key=lambda x: x[1], reverse=True)
	else:
		Dic_list = sorted(bestcons.items(), key=lambda x: x[1], reverse=True)
	# print Dic_list	
	First = True
	for tre in Dic_list:
		root = tree_reader.read_tree_string(tre[0])
		if First:
			maintree = root
			First = False
		else:
			consbp = get_biparts(root)[0]
			maintreebps = get_biparts(maintree)
			if conflictbp(consbp,maintreebps): continue
			if check_bp_in_bps(consbp,maintreebps): continue
			# print tre[0]
			leftclade = consbp.left
			rightclade = consbp.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if not mrca_right == maintree: 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			# nd.length = float(geneMLtrees_sumLLS)-tre[1]
			mrca.add_child(nd)
		# print maintree.get_newick_repr(True)+";"
	if method == "4" or method == "5":
		for c in universal_support_bp:
			leftclade = c.left
			rightclade = c.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if len(leftclade) >= len(rightclade): 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			# nd.length = float(geneMLtrees_sumLLS)-tre[1]
			mrca.add_child(nd)			
	with open(curDIR+"consensus_method"+method+".tre", "w") as outfile:
		# outfile.write(maintree.get_newick_repr(True)+";\n")
		outfile.write(maintree.get_newick_repr()+";\n")

			