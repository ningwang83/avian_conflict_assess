#! /usr/bin/env python

# rescale tip height to make the tree ultrametric
# root time = 100MYR, Ave generation time = 8.67 based on 48 avian species
# assume effective population size is about 500000-1000000, the tree height is about 6-12, I chose 10

import tree_reader
import os,sys
from node import Node

treeheight = 10.0 # arbitrarily set a total tree height 

if len(sys.argv) != 3:
	print "python "+sys.argv[0]+" speciestree outtree"
	sys.exit(0)

sptre,outname = sys.argv[1:]
curDIR = os.getcwd()+"/"

with open(sptre,"rU") as infile:
	trestr = infile.readline()
	root = tree_reader.read_tree_string(trestr)

for tip in root.leaves():
	cur = tip
	h = 0
	while True:
		cur = cur.parent
		if cur != None:
			h += cur.length
		else:
			break
	print h
	tip.length = treeheight - h

with open(curDIR+outname,"w") as outfile:
	outfile.write(root.get_newick_repr(True)+";")