import sys
import os
import os.path

cmd = "iqtree -nt 12 -s SEQ -pre NAME -m GTR+G -bb 1000 "

if __name__ == "__main__":
    if len (sys.argv) != 3:
        print "python "+sys.argv[0]+" dir nthreads"
        sys.exit(0)
    st = sys.argv[1]
    thread = sys.argv[2]
    if st[-1] != "/":
        st += "/"
    for i in os.listdir(st):
        os.system(cmd.replace("12",thread).replace("SEQ",st+i).replace("NAME",i))
