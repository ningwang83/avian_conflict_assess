#! /usr/bin/env python

"""
Simulating gene trees with a species tree that different edges have different population size
"""

import os,sys
import random
import dendropy# must be Dendropy 4.2.0
from dendropy.calculate import treecompare


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" truetreefile speciestree outfile"
		sys.exit(0)
	truetree,sptree,outname = sys.argv[1:]
	outfile = open(outname,"w")
	true_gene_tree_list = []
	#read str for tree_gene_tree_list
	with open(truetree,"rU") as intree:
		for line in intree:
			treestr = line.strip()[5:].replace("_1","")
			true_gene_tree_list.append(treestr)
	with open(sptree,"rU") as input:
		sptree = input.readline().strip()			
	#read trees and compare them using RF distance
	tns = dendropy.TaxonNamespace()
	tree1 = dendropy.Tree.get(
		data=sptree,
		schema="newick",
		taxon_namespace=tns)
	tree1.encode_bipartitions()
	for index in range(0,500):
		true = true_gene_tree_list[index]
		tree2 = dendropy.Tree.get(
				data=true,
				schema="newick",
				taxon_namespace=tns)
		tree2.encode_bipartitions()
		dist = treecompare.symmetric_difference(tree1, tree2)
		outfile.write(str(dist)+"\n")
		
	outfile.close()