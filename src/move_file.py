#! /usr/bin/env python

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python move_file.py inDIR outDIR file_end"
		sys.exit() 
	inDIR,outDIR,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR) + "/"
	outDIR = os.path.abspath(outDIR) + "/"
	treelist = []
	for f in os.listdir(inDIR):
		if not f.endswith(file_end): continue
		fileID = f.strip().split(".")[0]
		treelist.append(fileID)
	for f in os.listdir(inDIR):
		fID = f.strip().split(".")[0]
		if fID in treelist:
			cmd = "mv "+inDIR+f+" "+outDIR
			os.system(cmd)

