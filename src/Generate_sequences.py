#! /usr/bin/env python

"""base frequency and Gammar distribution alpha parameter
and GTR rate all simulated based on real dataset that calculated from Jarvis data
all the parameters are precalculated and round to contain four digit for density simulation
This script can be run correctly in Linux system, no my Mac"""

import os,sys
import tree_reader
from node import Node

import random
import dendropy# must be Dendropy 4.2.0
from dendropy.simulate import treesim
from dendropy.model import reconcile
from dendropy.interop import seqgen
from dendropy.datamodel.charmatrixmodel import CharacterMatrix
import glob
import subprocess
import numpy as np

# the value of pdic is [mu,sigma,mean,std]

def gen_basefreq(datatype,pdic): # pdic is the paramter dictionary
	GCsigma = pdic["GC"][1]
	GCmu = pdic["GC"][0]
	Astd = pdic["A"][3]
	Amean = pdic["A"][2]
	Asigma = pdic["A"][1]
	Amu = pdic["A"][0]
	Gsigma = pdic["G"][1]
	Gmu = pdic["GC"][0]
	while True:
		if datatype == "intron":
			b = np.random.lognormal(Amu, Asigma, 1)
			baseA = round(b[0],3)
		else:
			b = np.random.normal(Amean,Astd,1)
			baseA = round(b[0],3)
		a = np.random.lognormal(GCmu, GCsigma, 1)
		baseGC = round(a[0],3)
		baseAT = 1.0-baseGC
		baseT = round((baseAT-baseA),3)
		c = np.random.lognormal(Gmu, Gsigma, 1)
		baseG = round(c[0],3)
		baseC = round((baseGC-baseG),3)
		basefreq = [baseA, baseC, baseG, baseT]
		sumneg = sum(n <= 0 for n in basefreq)
		if sumneg == 0:
			return basefreq
			break

def Gshape(datatype,pdic): # alpha parameter
	if datatype == "exon":
		mu = pdic["alpha"][0]
		sigma = pdic["alpha"][1]
		p = np.random.lognormal(mu, sigma, 1)
	else:
		mean = pdic["alpha"][2]
		std = pdic["alpha"][3]
		p = np.random.normal(mean,std, 1)
	alpha = round(p[0],3)
	if alpha > 0:
		return alpha
	else:
		Gshape(datatype,pdic)


def GTR(pdic):
	lgtr = ["ac","ag","at","cg","ct"]
	mean = []
	std = []
	for rate in lgtr:
		m = pdic[rate][2]
		mean.append(m)
		s = pdic[rate][3]
		std.append(s)
	gtr_rate = []
	for i in range(0,5):
		while True:
			p = np.random.normal(mean[i],std[i], 1)
			par = round(p[0],3)
			if par > 0:
				gtr_rate.append(par)
				break
	gtr_rate.append(1.0)
	print gtr_rate
	return gtr_rate

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" scaledsimtreeDIR Summed_parameter file_start"
		print "datatype: exon, intron, or UCE, numgenes 500-1000"
		sys.exit(0)
	simDIR,pfile,filestart = sys.argv[1:]
	CurDIR = os.getcwd()+"/"
	simDIR = os.path.abspath(simDIR)+"/"
	
	# read pfile and gather parameter distribution information
	Paradic = {}
	First = True
	with open(pfile,"rU") as handel:
		for l in handel:
			if First:
				First = False
				continue
			else:
				line = l.strip().split("\t")
				pname = line[0]
				mu = float(line[1])
				sigma = float(line[2])
				mean = float(line[3])
				std = float(line[4])
				Paradic[pname] = [mu,sigma,mean,std]
	datatype = pfile.split("_")[-1]
	# read simtrees and generate gene_data alignment (800-3000bp)
	# from Dendropy Github find the seqgen.py file to get all the defined function
	for i in os.listdir(simDIR):
		if not i.startswith(filestart): continue
		simnum = i.split("_")[-1]
		if not os.path.exists("Seqgen_"+simnum):
			os.makedirs("Seqgen_"+simnum)
		gene_trees = dendropy.TreeList.get(path=simDIR+i,schema="newick")
		count = 1
		for tre in gene_trees:
			# calculate GC content, varies among genes
			basefreq = gen_basefreq(datatype,Paradic)
			# generate alignments
			s = seqgen.SeqGen()
			s.scale_branch_lens = 1 # modified properly I have rescaled the internodes already
			s.char_model = seqgen.SeqGen.GTR
			s.gamma_shape = Gshape(datatype,Paradic)
			s.gamma_cats = 4
			s.state_freqs = basefreq #A #C #G #T
			s.general_rates = GTR(Paradic) # ac ag at cg ct gt
			s.seq_len = random.randint(800,3000)
			d1 = s.generate(tre)
			#print(len(d1.char_matrices))
			seq = d1.char_matrices[0]
			with open(CurDIR+"Seqgen_"+simnum+"/"+"SeqGen."+datatype+"."+str(count),"w") as outfile:
				outfile.write(seq.as_string("fasta"))
			count += 1
