#! usr/bin/env python
# tree must be rooted

import node,tree_reader,os,sys,math
from tree_utils import *

def check_include(namelist,outgroup):
	count = 0
	for i in namelist:
		if not i in outgroup:
			count += 1
			break
	if count == 0:
		return True
	else:
		return False

if len(sys.argv) != 6:
	print "usage: python brlen_dis.py DIR file_end leftcutoff rightcutoff inter/tip"
	sys.exit()

WORKDIR,file_end,leftcutoff,rightcutoff,argu = sys.argv[1:]
WORKDIR = os.path.abspath(WORKDIR)+"/"

#internode_list = []
outgroup_list = ['Homo_sapiens','Chelonia_mydas','Alligator_mississippiensis','Anolis_carolinensis']

for i in os.listdir(WORKDIR):
	if not i.endswith(file_end): continue
	with open(WORKDIR+i,"r") as infile:
		intree = tree_reader.read_tree_string(infile.readline())
		alltip = set(intree.lvsnms())
	for node in intree.iternodes():
		if argu == "inter":
			if node.istip or node == intree: continue
			if node.length >= float(leftcutoff) and node.length <= float(rightcutoff):
				front = set(node.lvsnms())
				back = alltip - front
				finclude = check_include(front,outgroup_list)
				binclude = check_include(back,outgroup_list) # tree must be rooted
				if not binclude and not finclude:
					print i
					#internode_list.append(node.length)		
		elif argu == "tip":
			if not node.istip: continue
			#if node.length >= float(leftcutoff) and node.length <= float(rightcutoff):
			if node.length >= float(leftcutoff) and node.length <= float(rightcutoff) and not node.label in outgroup_list:
				print i
				#internode_list.append(node.length)




