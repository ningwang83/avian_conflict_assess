#! /usr/bin/env python

# must use ete2 implemented Tree and Node class.
# I must use ete2 as ete3 cannot load module correctly

import os,sys
from ete2 import Tree,faces,TreeStyle,COLOR_SCHEMES,NodeStyle,SVG_COLORS

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" WORKDIR species_tree node.keyfile conflict.histfile num_genes"
		sys.exit(0)
	WORKDIR, species_tree,nodekeyfile,hist_file,gene_num = sys.argv[1:]
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	gene_num = int(gene_num)
	
	# readin the conflict.histfile and generate pie_chart data for each node
	pie_charts = {}
	concord_conf = {}
	with open(hist_file,"rU") as handle:
		for l in handle:
			item = l.strip().split(",")
			sum_gene = float(item.pop(-1))
			nodename = item.pop(0)[4:]
			concord = round(float(item.pop(0)),0)
			total_conflict = round((sum_gene - concord),0)
			if len(item) > 0:
				most_conflict = max([float(x) for x in item])
			else:
				most_conflict = 0.0
			concord_per = (concord/gene_num) * 100 
			most_conflict_per = (most_conflict/gene_num) * 100
			other_conflict = (total_conflict - most_conflict) / gene_num * 100
			the_rest = (gene_num - concord - total_conflict) / gene_num * 100
			
			pie_charts[nodename] = [concord_per,most_conflict_per,other_conflict,the_rest]
			concord_conf[nodename] = [int(concord),int(total_conflict)]
	
	# readin the node.keyfile and generate key for corresponding each nodes
	node_keys = {}
	with open(nodekeyfile,"rU") as infile:
		for l in infile:
			nodenumber = l.strip().split(" ")[0]
			leave_str = l.strip().split(" ")[1].replace("(","").replace(")","")
			node_leaves = leave_str.split(",")
			node_keys[nodenumber] = node_leaves
	
	# readin the rooted species tree and draw pie chart on corresponding nodes
	sortnodes =  sorted(node_keys.keys())
	root = Tree(species_tree)	
	#treefile = open(species_tree,"rU")
	#treestr = treefile.readline()
	#root = tree_reader.read_tree_string(treestr)
	for node in sortnodes:
		MRCA = root.get_common_ancestor(set(node_keys[node]))
		pie = faces.PieChartFace(pie_charts[node],colors = ["blue","springgreen","red","silver"],width=50, height=50)
		pie.opacity = 0.5
		MRCA.add_face(pie,column=0,position="branch-right")
		concord_text = faces.TextFace(str(concord_conf[node][0])+'  ',fsize=20)
		conf_text = faces.TextFace(str(concord_conf[node][1])+'  ',fsize=20)
		# add the text to nodes
		MRCA.add_face(concord_text,column=0,position="branch-top")
		MRCA.add_face(conf_text,column=0,position="branch-bottom")
	
	for n in root.traverse():
		if n.is_leaf():
			F = faces.TextFace(' '+n.name,fsize=20,fstyle='italic')
			n.add_face(F,column=0)
			
	
	# visualize tree
	nstyle = NodeStyle()
	nstyle["size"] = 0
	for n in root.traverse():
		n.set_style(nstyle)
	
	ts = TreeStyle()
	ts.show_leaf_name = False
	#ts.show_branch_length = True
	ts.extra_branch_line_color = "orange"
	ts.mode = "r"
	#root.ladderize(direction=1)
	root.render(WORKDIR+"conflic_pie_chart.svg",tree_style=ts)

