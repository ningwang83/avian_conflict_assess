#! /usr/bin/env python
import os,sys
import tree_reader
from biparts import Bipart,get_biparts
from node import Node

"""copmare the rooted ML tree with each constrees and calculate the distribution of bipartitions
in these gene trees, export a dictionary for each bipartitions the key is the bipartition and the value
is the number of gene trees have that bipartition, use this to make a pie chart"""

"""root tree command: for i in *.treefile; do pxrr -t $i -g Homo_sapiens,Chelonia_mydas,Anolis_carolinensis,
Alligator_mississippiensis,Struthio_camelus,Tinamou_guttatus,Anas_platyrhynchos,Gallus_gallus,
Meleagris_gallopavo -r -o ../roottree/$i.rr;done"""

# a rooted tree make it much easier to do the analyses

file_end = ".rr"

def determine_bp(node,alltips):
	front = set(node.lvsnms())
	back = set(alltips-front)
	if len(front) <= len(back):
		left = front
		right = back # left should have less species
	else:
		left = back
		right = front
	return left,right

def find_equalbp(bp,bpdic): # exactly the same only for the sister clade, deal with missing data in right side of the clade
	bplist = bpdic.keys()
	count = 0
	for b in bplist:
		if sorted(b.left) == sorted(bp.left):
			bpdic[b] += 1
			count += 1
			break
	if count == 0:
		bpdic[bp] = 1
	return bpdic

def find_maxnode(root,taxaset):
	alltips = set(root.lvsnms())
	if len(taxaset) == 1:
		if not list(taxaset)[0] in alltips:
			return 0,0
		else:
			for l in root.leaves():
				if l.label == list(taxaset)[0]:
					p = l.parent
					frontptips = set(p.lvsnms())
					backptips = set(alltips-frontptips)
					return Bipart(frontptips-taxaset,backptips),p # *******
	else: # this is for the basal neoaves
		maxinter = 0
		curbp = Bipart(set(),set())
		curnode = Node()
		First = True
		for n in root.iternodes():
			if len(n.children) == 0 or n == root: continue
			nleft,nright = determine_bp(n,alltips)
			mlbp = Bipart(nleft,nright) # left always have less taxa
			overlaptaxa = len(taxaset.intersection(nleft))
			dif = len(nleft)-overlaptaxa
			if dif == 0:
				if overlaptaxa > maxinter:
					maxinter = overlaptaxa
					curbp = mlbp
					curnode = n
		curfront = set(curnode.lvsnms())
		curback = set(alltips-curfront)
		if len(taxaset.intersection(curfront)) > 0 and len(taxaset.intersection(curback)) > 0:
			return 1,1
		elif len(taxaset.intersection(curfront)) == 0 and len(taxaset.intersection(curback)) > 0:
			child1,child2 = curnode.children[0],curnode.children[1]
			ch1front = set(child1.lvsnms())
			ch2front = set(child2.lvsnms())
			ch1back = set(alltips-ch1front)
			ch2back = set(alltips-ch2front)
			if len(ch1front) <= len(ch2front):
				return Bipart(ch2back-curback,ch2front),child2 # minus the taxaset so avoid conflict from outgroups, specific deal with basal
			else:
				return Bipart(ch1back-curback,ch1front),child1 # *******
		elif len(taxaset.intersection(curfront)) > 0 and len(taxaset.intersection(curback)) == 0:
			curp = curnode.parent
			pleft = set(curp.lvsnms())
			pright = set(alltips-pleft)
			return Bipart(pleft-curfront,pright),curp # *******

if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" Constreefile MLgeneDIR support"
	sys.exit(0)

consfile,MLtreeDIR,support = sys.argv[1:]
MLtreeDIR = os.path.abspath(MLtreeDIR)+"/"
curDIR = os.getcwd()+"/"
support = int(support)

# setup the focal edge
Outgroup = set(["Struthio_camelus","Tinamou_guttatus","Anas_platyrhynchos","Gallus_gallus","Meleagris_gallopavo"])
hoazin = set(["Ophisthocomus_hoazin"])
owl = set(["Tyto_alba"])
edgelist = [Outgroup,hoazin,owl]

# read in each constraints and setup dictionary for all potential 
# consistent or conflict bipartitions in the ML tree

intree = open(consfile,"rU")
edge = 0
for l in intree: # each constraint tree only has one bipartition
	cons = tree_reader.read_tree_string(l.strip())
	edge += 1
	print edge
	out = set(cons.lvsnms())
	for i in cons.iternodes():
		if len(i.children) != 0 and i != cons:
			consleft,consright = determine_bp(i,out)
			consleft = set(consleft - consleft.intersection(edgelist[edge-1])) # *******
			break
	consbp = Bipart(consleft,consright)
	bpdic = {}
	bpdic[consbp] = 0
	# read ML trees from MLtreeDIR
	unsupport = 0
	notaxa = 0
	wrongout = 0
	for mltre in os.listdir(MLtreeDIR):
		#print mltre
		if not mltre.endswith(file_end): continue
		with open(MLtreeDIR+mltre,"rU") as infile:
			oneline = infile.readline()
			root = tree_reader.read_tree_string(oneline)
		tarbp,tarnode = find_maxnode(root,edgelist[edge-1])
		if tarbp == 0 and tarnode == 0:
			notaxa += 1
			print "missingtaxa\t"+mltre
			continue
		if tarbp == 1 and tarnode ==1:
			wrongout += 1
			print "wrongout\t"+mltre
			continue
		if tarnode.label != "" and int(tarnode.label) < support:
			unsupport += 1
		else:
			bpdic = find_equalbp(tarbp,bpdic)	
	with open(curDIR+"Consedge"+str(edge), "w") as outfile:
		uninform = 0
		for key in bpdic.keys():
			# outfile.write(key.newick()+"\t"+str(bpdic[key])+"\n")
			if bpdic[key] <= 1:
				uninform += bpdic[key]
			else:
				outfile.write(key.newick()+"\t"+str(bpdic[key])+"\n")
		outfile.write("uninformative\t"+str(uninform)+"\n")
		outfile.write("unsupport\t"+str(unsupport)+"\n")
		outfile.write("wrongout\t"+str(wrongout)+"\n")
		outfile.write("missingtaxa\t"+str(notaxa))