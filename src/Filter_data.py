#! /usr/bin/env python

"""filter Jarvis data"""

import os,sys
from stat import *
import shutil

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" inDIR file_end outDIR cutoff len"
		print "make an outDIR first, cutoff is the min percentage of nonalign/align, 0.1-0.3"
		print "len is the min length of the aligned sequences, 500"
		sys.exit(0)
	inDIR,file_end,outDIR,minper,minlen = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	for fasta in os.listdir(inDIR):
		if not fasta.endswith(file_end): continue
		seqDict = read_fasta(inDIR+fasta)
		for key in seqDict.keys():
			seqalign = seqDict[key]
			alignseqlen = float(len(seqalign))
			#print alignseqlen
			if alignseqlen < int(minlen): 
				print fasta
				shutil.move(inDIR+fasta,outDIR+fasta)				
				break
			seqnonalign = seqalign.replace("?","")
			seqnonalign = seqnonalign.replace("-","")
			nonalignseqlen = float(len(seqnonalign))
			#print nonalignseqlen
			perc = float(nonalignseqlen/alignseqlen)
			if perc < float(minper):
				del seqDict[key]
				continue
		print fasta, len(seqDict.keys())
		if os.path.exists(inDIR+fasta) and len(seqDict.keys()) < 42:
			shutil.move(inDIR+fasta,outDIR+fasta)

