#! /usr/bin/env python

import os,sys

if len(sys.argv) != 3:
	print "python "+sys.argv[0]+" Basalhypo.txt outname"
	sys.exit()

hypo,outname = sys.argv[1:]
curDIR = os.getcwd()+"/"
infile = open(hypo,"rU")
outfile = open(curDIR+outname,"w")

First = True
for l in infile:
	if First:
		cons = l.strip().split("\t")[1:-1]
		outfile.write(l.strip()+"\tbestone\tbest\tsecondbest\tdiffbestsecondbest\n")
		First = False
	else:
		genename = l.strip().split("\t")[0]
		score = l.strip().split("\t")[1:-1]
		rescore = []
		for i in score:
			if i == "-":
				i = -999999999
				rescore.append(i)
			else:
				rescore.append(float(i))
		zipScons = dict(zip(rescore,cons)) # score must be different
		sortscore = sorted(rescore)
		best = sortscore[-1]
		secbest = sortscore[-2]
		diff = round((best-secbest),4)
		bestone = zipScons[best]
		outfile.write(l.strip()+"\t"+bestone+"\t"+str(best)+"\t"+str(secbest)+"\t"+str(diff)+"\n")
infile.close()
outfile.close()
