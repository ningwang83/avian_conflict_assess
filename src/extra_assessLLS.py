#! /usr/bin/env python

import os,sys
import glob

raxml = "raxmlHPC-PTHREADS-AVX "

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" fasDIR constrainttreefile outname fasta_end"
		sys.exit(0)
	trim_fasDIR,constres,outname,fasta_end = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	
	# make unique_constraints list
	unique_constraints = []
	with open(curDIR+constres,"rU") as infile:
		for l in infile:
			tre = l.strip().split("\t")[1]
			unique_constraints.append(tre)
	
	outf  = open(curDIR+outname,"w")
	outf.write("gene\t"+"\t".join(str(x) for x in range(0,len(unique_constraints)))+"\n")
	for fas in os.listdir(trim_fasDIR):
		if not fas.endswith(fasta_end): continue
		locus = fas.split(".")[0]
		locus_LLS_list = []
		count = 182 # complement for more constraints
		for uc in unique_constraints:
			temptre = open(curDIR+"temp.tre","w")
			temptre.write(uc)
			temptre.close()
			cmdraxml = raxml+"-T 8 -p 12345 -m GTRGAMMA -g "+curDIR+"temp.tre"+" -s "+trim_fasDIR+fas+" -n "+locus+".cons"+str(count)+" | grep 'best tree'"
			print cmdraxml			
			LLSstr = os.popen(cmdraxml).readlines()
			LLS = LLSstr[0].strip().split(" ")[-1]
			locus_LLS_list.append(LLS)
			for f in glob.glob(curDIR+"RAxML_log.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_info.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_result.*"):
				os.remove(f)
			count += 1
		outf.write(locus+"\t"+"\t".join(locus_LLS_list)+"\n")
	outf.close()