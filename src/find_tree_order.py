#! /usr/bin/env python
import os,sys
import tree_reader
from node import Node
from biparts import Bipart,get_biparts


basal = ["Struthio_camelus","Tinamou_guttatus","Anas_platyrhynchos","Meleagris_gallopavo","Gallus_gallus","Columba_livia","Mesitornis_unicolor","Pterocles_gutturalis","Phoenicopterus_ruber","Podiceps_cristatus"]
hoazin = ["Ophisthocomus_hoazin","Gavia_stellata","Phalacrocorax_carbo","Nipponia_nippon","Pelecanus_crispus","Egretta_garzetta","Fulmarus_glacialis","Pygoscelis_adeliae","Aptenodytes_forsteri","Phaethon_lepturus","Eurypyga_helias","Balearica_regulorum","Charadrius_vociferus"]
Owleagle = ["Tyto_alba","Haliaeetus_albicilla","Haliaeetus_leucocephalus","Cathartes_aura"]
Owlmousebird = ["Tyto_alba","Colius_striatus"]
MouseCariama = ["Colius_striatus","Cariama_cristata"]

Dicedge = {}
Dicedge["basal"] = basal
Dicedge["hoazin"] = hoazin
Dicedge["Owleagle"] = Owleagle
Dicedge["Owlmousebird"] = Owlmousebird
Dicedge["MouseCariama"] = MouseCariama

if len(sys.argv) != 2:
	print "python "+sys.argv[0]+" TestingConstraints"
	sys.exit(0)

treefile = sys.argv[1]
with open(treefile,"rU") as infile:
	for line in infile:
		trenum = line.strip().split("\t")[0]
		treestr = line.strip().split("\t")[1]
		root = tree_reader.read_tree_string(treestr)
		consbps = get_biparts(root)
		consbp = consbps[0]
		consleft = consbp.left
		consright = consbp.right
		for key in Dicedge.keys():
			if sorted(consleft) == sorted(set(Dicedge[key])) or sorted(consright) == sorted(set(Dicedge[key])):
				print key+"\t"+trenum
				break
