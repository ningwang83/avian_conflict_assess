#! /usr/bin/env python

"""this script read in Parameter.csv file and calculate the mean, std, logmean, logstd for 
each parameter for exon, intron and UCE
files header [0] loci; [1] alpha; [2] ac; [3] ag; [4] at; [5] cg; [6] ct; [8] A; [9] C; [10] G [11] T"""

import os,sys
import csv
import numpy as np

if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" WORKDIR file_end outfile"
	print "files are Parameter_exon/intron/UCE.csv"
	sys.exit()

WORKDIR,file_end,outname = sys.argv[1:]
WORKDIR = os.path.abspath(WORKDIR)+"/"

for f in os.listdir(WORKDIR):
	if not f.endswith(file_end): continue
	datatype = f.split(".")[0]
	datatype = datatype.split("_")[-1]
	par_dic = {}
	par_dic["alpha"] = []
	par_dic["ac"] = []
	par_dic["ag"] = []
	par_dic["at"] = []
	par_dic["cg"] = []
	par_dic["ct"] = []
	par_dic["A"] = []
	par_dic["C"] = []
	par_dic["G"] = []
	par_dic["GC"] = []
	with open(WORKDIR+f) as infile:
		reader = csv.reader(infile)
		header = next(reader)
		for row in reader:
			par_dic["alpha"].append(float(row[1]))
			par_dic["ac"].append(float(row[2]))
			par_dic["ag"].append(float(row[3]))
			par_dic["at"].append(float(row[4]))
			par_dic["cg"].append(float(row[5]))
			par_dic["ct"].append(float(row[6]))
			par_dic["A"].append(float(row[8]))
			par_dic["C"].append(float(row[9]))
			par_dic["G"].append(float(row[10]))
			par_dic["GC"].append(float(row[9])+float(row[10]))
	outfile = open(WORKDIR+outname+"_"+datatype,"w")
	outfile.write("parameter\tmu\tsigma\tmean\tstd\n")
	for k in par_dic.keys():
		p = par_dic[k]
		sigma = np.std(np.log(p))
		mu = np.mean(np.log(p))
		mean = np.mean(p)
		std = np.std(p)
		outfile.write(k+"\t"+str(mu)+"\t"+str(sigma)+"\t"+str(mean)+"\t"+str(std)+"\n")
	outfile.close()

		
	
