import os,sys
import glob
raxml = "raxmlHPC-PTHREADS-AVX "
fasDIR = os.path.abspath("../rename/")+"/"
curDIR = os.getcwd()+"/"
LLS_Best_Dict = {}
Sum_LLS_best = 0
for fa in os.listdir(fasDIR):
    if not fa.startswith("SeqGen"): continue
    if fa.endswith("reduced"): continue
    print fa
    #locus = fa[6:]
    locus = fa.split(".")[-1]
    if os.path.exists(curDIR+"RAxML_bestTree."+fa):
        raxmlcmd = raxml+"-f e -s "+fasDIR+fa+" -t "+curDIR+"RAxML_bestTree."+fa+" -m GTRGAMMA -n "+locus+" | grep 'Final GAMMA  likelihood'"
        LLS_best_str = os.popen(raxmlcmd).readlines()
        LLS_best_score = LLS_best_str[0].strip().split(": ")[-1]
    LLS_Best_Dict[locus] = LLS_best_score
    Sum_LLS_best += float(LLS_best_score)
    for f in glob.glob(curDIR+"RAxML_log.*"):
            os.remove(f)
    for f in glob.glob(curDIR+"RAxML_info.*"):
            os.remove(f)
    for f in glob.glob(curDIR+"RAxML_result.*"):
            os.remove(f)
    for f in glob.glob(curDIR+"RAxML_parsimony*"):
            os.remove(f)
    for f in glob.glob(curDIR+"RAxML_binaryModelParameters*"):
            os.remove(f)
print Sum_LLS_best

for k in LLS_Best_Dict.keys():
    print k, LLS_Best_Dict[k]

for f in glob.glob(fasDIR+"*.reduced"):
    os.remove(f)

