#! /usr/bin/env python

"""use this script to extract trees with proper outgroups"""

import os,sys
import shutil

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python "+sys.argv[0]+" infile inDIR outDIR"
        print "infile is the output made from assess outgroup"
        sys.exit(0)
    
    Result_file,inDIR,outDIR = sys.argv[1:]
    inDIR = os.path.abspath(inDIR)+"/"
    outDIR = os.path.abspath(outDIR)+"/"
    count = 0
    list = []
    keeplist = []
    with open(Result_file,"rU") as handle:
         for li in handle:
                 intron = li.strip()
                 if not intron in list:
                         list.append(intron)
                 else: 
                         count += 1
                         keeplist.append(intron)
    #print count
    #print keeplist
    for i in keeplist:
        shutil.move(inDIR+i+".contree",outDIR+i+".contree")
