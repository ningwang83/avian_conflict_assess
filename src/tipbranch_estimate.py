#! /usr/bin/env python

# use this to calculate tip length

import os,sys
import numpy as np
import tree_reader
from node import Node

def diff(list1,exclist):
	diff = False
	for i in list1:
		if not i in exclist:
			diff = True
			break
	return diff
			
#exclude outgroups
exclude = ["Alligator_mississippiensis","Chelonia_mydas","Anolis_carolinensis","Homo_sapiens"]

if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" treeDIR file_end outfile"
	sys.exit()

treeDIR,file_end,outname = sys.argv[1:]
treeDIR = os.path.abspath(treeDIR)+"/"
curDIR = os.getcwd()+"/"
treelenf = open(curDIR+"treelength4gene.out","w")
treelenf.write("gene\ttreelen\tinterlen\ttotaNIngroupbr\n")

treelen = []
interavelen = {}
tipbrlen = {}
for tree in os.listdir(treeDIR):
	if not tree.endswith(file_end): continue
	treename = tree.split(".")[0]
	treelength = 0.0
	interall = 0.0
	count = 0
	splen = {}
	tipnum = 0
	with open(treeDIR+tree,"rU") as handle:
		treestr = handle.readline()
		root = tree_reader.read_tree_string(treestr)
	for node in root.iternodes():
		if not node.istip:
			if node == root: continue
			leaves = node.leaves()
			labels = [j.label for j in leaves]
			diflabel = diff(labels,exclude) # exclude internal brlen from outgroup
			if diflabel == False: continue
			treelength += node.length
			interall += node.length
			count += 1
		else:
			spe = node.label
			if spe in exclude: continue
			tipnum += 1
			brlen = node.length
			splen[spe] = brlen
			treelength += brlen
			if not spe in tipbrlen.keys():
				tipbrlen[spe] = [brlen]
			else:
				tipbrlen[spe].append(brlen)
	treelen.append(treelength)
	treelenf.write(tree+"\t"+str(treelength)+"\t"+str(interall)+"\t"+str(count+tipnum)+"\n")
	aveinter = interall/count
	key = treename+":"+str(aveinter)
	interavelen[key] = splen
treelenf.close()

outfile = open(curDIR+outname,"w")

#treelength summerize:
sortTlen = sorted(treelen)
medianTlen = np.median(sortTlen)
meanTlen = np.mean(sortTlen)
stdTlen = np.std(sortTlen)
longest = sortTlen[-1]
shortest = sortTlen[0]
outfile.write("longest\tshortest\tmediantree\tmeanTlen\tstdTlen\n")
outfile.write(str(longest)+"\t"+str(shortest)+"\t"+str(medianTlen)+"\t"+str(meanTlen)+"\t"+str(stdTlen)+"\n")

outfile.write("species\tmedianbr\tmeanbr\tstdbr\tmax\tmin\n")
for sp in tipbrlen.keys():
	brlendis = sorted(tipbrlen[sp])
	maxx = brlendis[-1]
	minn = brlendis[0]
	median = np.median(brlendis)
	mean = np.mean(brlendis)
	std = np.std(brlendis)
	outfile.write(sp+"\t"+str(median)+"\t"+str(mean)+"\t"+str(std)+"\t"+str(maxx)+"\t"+str(minn)+"\n")
outfile.close()

# output interave len for each tree and species tip brlen
with open(curDIR+"aveInter4rescale_simtree.out","w") as outhandle:
	for inter in interavelen.keys():
		outhandle.write("\n"+str(inter)+"\t")
		for sp in sorted(interavelen[inter].keys()):
			l = interavelen[inter][sp]
			outhandle.write(sp+":"+str(l)+"\t")

			
			
