#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts

"""
for csv file, the column are separated by comma:
0-gene,1-Ln(cons_0),2-Ln(cons_0_conf_0),3-Ln(cons_0_conf_1),
4-Ln(cons_0_conf_2),5-Ln(cons_0_conf_3),6-bestone,7-Ln(bestone),8-Ln(secondbest),
9-Ln(diffbestsecondbest),10-Ln(ml-iqtree-noconstraint) [optional]

"""

def generate_bp(treefile):
	intree = open(treefile,"rU")
	treeline = intree.readline().strip()
	MLtree = tree_reader.read_tree_string(treeline)
	MLbps = get_biparts(MLtree)
	intree.close()
	return MLbps

def Num_conflict_bp(targetbp,comparedbp):
	#targetbp is the bps in targeted tree
	#comparedbp is the bps in the ML gene tree or species tree
	tbp = targetbp
	cbp = comparedbp
	count = 0
	for i in tbp:
		conflict = True
		for j in cbp:
			if i.equal(j):
				conflict = False
				break
		if conflict:
			count += 1
	return count



if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" Basal/Hoazin/Owl.csv constreesDIR MLtreeDIR speciestree outfile"
		sys.exit(0)
	
	csv,consDIR,MLDIR,speciestree,outname = sys.argv[1:]
	consDIR = os.path.abspath(consDIR)+"/"
	MLDIR = os.path.abspath(MLDIR)+"/"
	curDIR = os.getcwd()+"/"
	species_bps = generate_bp(speciestree)
	outfile = open(curDIR+outname,"w")
	outfile.write("gene\tTotalbestonebp\tTotalMLbp\tconf_best_ML\tconf_best_species\tconf_ML_species\n")
	
	#read csv file, remember the bestone and conduct compare
	first = True
	index_best = -4 # get the bestone column
	with open(csv,"rU") as infile:
		for line in infile:
			if first:
				line_list = line.strip().split(",")
				index_best = line_list.index("bestone")
				first = False
			else:
				gene_name = line.strip().split(",")[0]
				bestcon = line.strip().split(",")[index_best]
				constree = gene_name+"___"+bestcon+".treefile"
				MLtree = gene_name+".treefile"
				# generate biparitions for traget and ML compared trees
				targetbps = generate_bp(consDIR+constree)
				comparedbps = generate_bp(MLDIR+MLtree)
				# compare bipartitions and calculated the number of conflict bp in target tree
				Total_tbp = str(len(targetbps))
				Total_cbp = str(len(comparedbps))
				conflict_bp = Num_conflict_bp(targetbps,comparedbps)
				conflict_best_species = Num_conflict_bp(targetbps,species_bps)
				conflict_ML_species = Num_conflict_bp(comparedbps,species_bps)
				outfile.write(gene_name+"\t"+Total_tbp+"\t"+Total_cbp+"\t"+str(conflict_bp)+"\t"+str(conflict_best_species)+"\t"+str(conflict_ML_species)+"\n")
	outfile.close()
		

