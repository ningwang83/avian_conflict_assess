#! /usr/bin/env python

# use this script to digest RF distance output from bp analyses
# `bp -rfp -w 100 -t treefile`
import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python org_RFdist.py infile matrixfile"
		sys.exit() 
	inname,matrixname = sys.argv[1:]
	# set up dictionary
	treedisDict = {}
	infile = open(inname,"rU")
	for l in infile:
		treepair = l.strip().split(": ")[0]
		dist = l.strip().split(": ")[1] # str
		tree1 = int(treepair.split(" ")[0])
		tree2 = int(treepair.split(" ")[1])
		treeset = tuple([tree1,tree2]) # only tuple can be used as dictionary key
		# print treeset
		if not tree1 in treedisDict.keys():
			treedisDict[tree1] = {}
			treedisDict[tree1][treeset] = dist
		else:
			treedisDict[tree1][treeset] = dist
	infile.close()
	matrixfile = open(matrixname,"w")
	for key in sorted(treedisDict.keys()):
		matrixfile.write(str(key)+",")
		trepairs = sorted(treedisDict[key].keys(),reverse=True)
		for keypair in trepairs:
			# print keypair # double check the order
			distance = treedisDict[key][keypair]
			matrixfile.write(distance+",")
		matrixfile.write("\n")
	matrixfile.close()