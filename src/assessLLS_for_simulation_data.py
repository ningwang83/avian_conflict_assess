#! /usr/bin/env python

import os,sys
#import tree_reader
#from biparts import Bipart,get_biparts
import glob
#from assessLLS_for_each_edge_on_completedata import *

raxml = "raxmlHPC-PTHREADS-AVX "

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" fasDIR unique_constraints"
		sys.exit(0)
	fasDIR,uniquecons = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	curDIR = os.getcwd()+"/"
	
	#conduct raxml analyses, build best tree and collect LLS scores
	LLS_Best_Dict = {}
	Sum_LLS_best = 0
	for fa in os.listdir(fasDIR):
		if not fa.startswith("SeqGen"): continue
		if fa.endswith("reduced"): continue
		#locus = fa[6:]
		locus = fa.split(".")[-1]
		if os.path.exists(curDIR+"RAxML_bestTree."+fa):
			raxmlcmd = raxml+"-f e -s "+fasDIR+fa+" -t "+curDIR+"RAxML_bestTree."+fa+" -m GTRGAMMA -n "+locus+" | grep 'Final GAMMA  likelihood'"
			LLS_best_str = os.popen(raxmlcmd).readlines()
			LLS_best_score = LLS_best_str[0].strip().split(": ")[-1]
		else:
			raxmlcmd = raxml+"-T 8 -p 12345 -s "+fasDIR+fa+" -m GTRGAMMA -n "+fa+" | grep 'best tree'"
			LLS_best_str = os.popen(raxmlcmd).readlines()
			LLS_best_score = LLS_best_str[0].strip().split(" ")[-1]
		#print LLS_best_str[0]
		LLS_Best_Dict[locus] = LLS_best_score
		#print LLS_best_score
		Sum_LLS_best += float(LLS_best_score)
		for f in glob.glob(curDIR+"RAxML_log.*"):
			os.remove(f)
		for f in glob.glob(curDIR+"RAxML_info.*"):
			os.remove(f)
		for f in glob.glob(curDIR+"RAxML_result.*"):
			os.remove(f)
		for f in glob.glob(curDIR+"RAxML_parsimony*"):
			os.remove(f)
		for f in glob.glob(curDIR+"RAxML_binaryModelParameters*"):
			os.remove(f)
	print Sum_LLS_best
	for f in glob.glob(fasDIR+"*.reduced"):
		os.remove(f)
	
	#run raxml for constraint tree analyses for each gene fasta
	unique_constraints = []
	with open(uniquecons,"rU") as handle:
		for line in handle:
			uc = line.strip().split("\t")[-1]
			unique_constraints.append(uc)
	
	outf  = open(curDIR+"LLSscore_simdata.out","w")
	outf.write("gene\t"+"\t".join(str(x) for x in range(0,len(unique_constraints)))+"\tbestML\n")
	for fas in os.listdir(fasDIR):
		if not fas.startswith("SeqGen"): continue
		if fas.endswith("reduced"): continue
		#locus = fas[6:]
		locus = fas.split(".")[-1]
		locus_LLS_list = []
		count = 0
		for uc in unique_constraints:
			temptre = open(curDIR+"temp.tre","w")
			temptre.write(uc)
			temptre.close()
			cmdraxml = raxml+"-T 8 -p 12345 -m GTRGAMMA -g "+curDIR+"temp.tre"+" -s "+\
					fasDIR+fas+" -n "+fas+".cons"+str(count)+" | grep 'best tree'"
			print cmdraxml			
			LLSstr = os.popen(cmdraxml).readlines()
			LLS = LLSstr[0].strip().split(" ")[-1]
			locus_LLS_list.append(LLS)
			for f in glob.glob(curDIR+"RAxML_log.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_info.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_result.*"):
				os.remove(f)
			count += 1
		outf.write(locus+"\t"+"\t".join(locus_LLS_list)+"\t"+LLS_Best_Dict[locus]+"\n")
	outf.close()


