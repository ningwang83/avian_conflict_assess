#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts
import glob


""" 
In Jarvis' provided trees, only TENT+outgroup and WGT trees contain the following outgroups
In this case, all data tested for conplete matrix need to be trimmed for outgroups
the gene trees generated before also need to be pruned for outgroup taxa
""" 

outgroups = ["Homo_sapiens","Alligator_mississippiensis","Chelonia_mydas","Anolis_carolinensis"]
tree_end = ".treefile"
fasta_end = ".fasta"
#fasta_end = ".intron"
raxml = "raxmlHPC-PTHREADS-AVX "

def reroot(oldroot, newroot):
	v = [] #path to the root
	n = newroot
	while 1:
		v.append(n)
		if not n.parent: break
		n = n.parent
	v.reverse()
	for i, cp in enumerate(v[:-1]):
		node = v[i+1]
		# node is current node; cp is current parent
		#print node.label, cp.label
		cp.remove_child(node)
		node.add_child(cp)
		cp.length = node.length
		cp.label = node.label
	return newroot

def remove_kink(node,curroot):
	#smooth the kink created by prunning
	if node == curroot and len(curroot.children) == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = reroot(curroot,curroot.children[1])
		else: curroot = reroot(curroot,curroot.children[0])
	# if root is a kink
	if node == curroot and len(curroot.children) == 1:
		kink = node
		curroot = reroot(curroot,curroot.children[0])
		curroot.remove_child(kink)
		node = curroot
		return node, curroot
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

def remove_tip(root,tip):
	node = tip.prune()
	node,root = remove_kink(node,root)
	return root

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

def generate_bp(treefile):
	intree = open(treefile,"rU")
	treeline = intree.readline().strip()
	MLtree = tree_reader.read_tree_string(treeline)
	MLbps = get_biparts(MLtree)
	intree.close()
	return MLbps

def get_cons_from_bp(bp):
	left = ",".join(bp.left)
	right = ",".join(bp.right)
	cons = "(("+left+"),"+right+");"
	return cons

def check_bp_in_bps(bp,bps):
	inbps = False
	for i in bps:
		if i.equal(bp):
			inbps = True
			break
	return inbps

def get_conf_from_canditre(bp,bps):
	conflict_list = []
	for i in bps:
		if i.conflict(bp):
			conftrestr = get_cons_from_bp(i)
			conflict_list.append(conftrestr)
	return conflict_list

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" fasDIR genetreeDIR Majorspecies_tree testingtreeDIR ConstraintsDIR"
		print "testingtreeDIR containts some species tree from Jarvis"
		sys.exit(0)
	fasDIR,gtDIR,speciestre,testtreDIR,consDIR = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	gtDIR = os.path.abspath(gtDIR)+"/"
	testtreDIR = os.path.abspath(testtreDIR)+"/"
	trim_fasDIR = fasDIR+"trimmedfas/"
	trim_gtDIR = gtDIR+"trimmedtre/"
	curDIR = os.getcwd()+"/"
	if not os.path.exists(consDIR):
		os.makedirs(consDIR)
	consDIR = os.path.abspath(consDIR)+"/"
	
	#make dir for trimmed fasta and corresponding gene tree
	if not os.path.exists(trim_fasDIR):
		os.makedirs(trim_fasDIR)
	if not os.path.exists(trim_gtDIR):
		os.makedirs(trim_gtDIR)
	
	#exclude outgroups from fastafile
	for fas in os.listdir(fasDIR):
		if not fas.endswith(fasta_end): continue
		fasDict = read_fasta(fasDIR+fas)
		outfas = open(trim_fasDIR+fas,"w")
		for key in fasDict.keys():
			if key in outgroups: continue
			else:
				outfas.write(">"+key+"\n"+fasDict[key]+"\n")
		outfas.close()
	
	#trim outgroups from gene trees
	for tre in os.listdir(gtDIR):
		if not tre.endswith(tree_end): continue
		intre = open(gtDIR+tre,"rU")
		treeline = intre.readline().strip()
		root = tree_reader.read_tree_string(treeline)		
		outtre = open(trim_gtDIR+tre,"w")
		for i in root.leaves():
			if i.label in outgroups:
				root = remove_tip(root,i)
		outtre.write(root.get_newick_repr(True)+";")
		intre.close()
		outtre.close()
	
	#calculate the LLS for the best gene tree
	LLS_Best_Dict = {}
	Sum_LLS_best = 0
	for fa in os.listdir(trim_fasDIR):
		if not fa.endswith(fasta_end): continue
		locus = fa.split(".")[0]
		raxmlcmd = raxml+"-f e -s "+trim_fasDIR+fa+" -t "+trim_gtDIR+fa+".treefile -m GTRGAMMA -n "+locus+" | grep 'Final GAMMA  likelihood'"
		LLS_best_str = os.popen(raxmlcmd).readlines()
		print LLS_best_str[0]
		LLS_best_score = LLS_best_str[0].strip().split(": ")[-1]
		LLS_Best_Dict[locus] = LLS_best_score
		#print LLS_best_score
		Sum_LLS_best += float(LLS_best_score)
		for file in glob.glob(curDIR+"RAxML_*"):
			os.remove(file)		
	print Sum_LLS_best
	
	#generate testing bps from testing tree files
	first = True
	testbps = []
	for testtre in os.listdir(testtreDIR):
		if first:
			testbps = generate_bp(testtreDIR+testtre)
			first = False
		else:
			trebps = generate_bp(testtreDIR+testtre)
			for i in trebps:
				if check_bp_in_bps(i,testbps) == False:
					testbps.append(i)

	#read species tree and get cons0 for each edge, combine all set(constraints), no duplication
	all_constraints = []
	speciestre_bps = generate_bp(speciestre)
	count = 0
	for bp in speciestre_bps:
		conflict_list = get_conf_from_canditre(bp,testbps)
		cons0 = get_cons_from_bp(bp)
		all_constraints.append(cons0)
		all_constraints = all_constraints + conflict_list
		with open(consDIR+"Constraints"+str(count),"a") as outfile:
			outfile.write(cons0+"\n")
			for c in conflict_list:
				outfile.write(c+"\n")
		count += 1
	unique_constraints = set(all_constraints)
	with open(curDIR+"all_unique_constraints.tre", "w") as outtrefile:
		num = 0
		for uc in unique_constraints:
			outtrefile.write(str(num)+"\t"+uc+"\n")
			num += 1

	#run raxml for constraint tree analyses for each gene fasta file for each constraint tree
	outf  = open(curDIR+"LLSscore.out","w")
	outf.write("gene\t"+"\t".join(str(x) for x in range(0,len(unique_constraints)))+"\tbestML\n")
	for fas in os.listdir(trim_fasDIR):
		if not fas.endswith(fasta_end): continue
		locus = fas.split(".")[0]
		locus_LLS_list = []
		count = 0
		for uc in unique_constraints:
			temptre = open(curDIR+"temp.tre","w")
			temptre.write(uc)
			temptre.close()
			cmdraxml = raxml+"-T 8 -p 12345 -m GTRGAMMA -g "+curDIR+"temp.tre"+" -s "+trim_fasDIR+fas+" -n "+locus+".cons"+str(count)+" | grep 'best tree'"
			print cmdraxml			
			LLSstr = os.popen(cmdraxml).readlines()
			LLS = LLSstr[0].strip().split(" ")[-1]
			locus_LLS_list.append(LLS)
			for f in glob.glob(curDIR+"RAxML_log.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_info.*"):
				os.remove(f)
			for f in glob.glob(curDIR+"RAxML_result.*"):
				os.remove(f)
			count += 1
		outf.write(locus+"\t"+"\t".join(locus_LLS_list)+"\t"+LLS_Best_Dict[locus]+"\n")
	outf.close()
	
