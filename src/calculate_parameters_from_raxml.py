#! /usr/bin/env python

import os,sys
import glob

raxml = "raxmlHPC-PTHREADS-AVX "
if len(sys.argv) != 4:
	print "python "+sys.argv[0]+" fasDIR file_end datatype"
	print "datatype = exon, intron, UCE"
	sys.exit(0)
fasDIR,file_end,datatype = sys.argv[1:]
fasDIR = os.path.abspath(fasDIR)+"/"
curDIR = os.getcwd()+"/"

for fa in os.listdir(fasDIR):
	if not fa.endswith(file_end): continue
	locus = fa.split(".")[0]
	if datatype == "exon":
		raxmlcmd = raxml+"-T 4 -p 12345 -s "+fasDIR+fa+" -m GTRGAMMA -n "+locus+".exon"
	elif datatype == "intron":
		raxmlcmd = raxml+"-T 8 -p 12345 -s "+fasDIR+fa+" -m GTRGAMMA -n "+locus+".intron"
	else:
		raxmlcmd = raxml+"-T 4 -p 12345 -s "+fasDIR+fa+" -m GTRGAMMA -n "+locus+".UCE"
	os.system(raxmlcmd)
	for f in glob.glob(curDIR+"RAxML_log.*"):
		os.remove(f)
	for f in glob.glob(curDIR+"RAxML_result.*"):
		os.remove(f)
	for f in glob.glob(curDIR+"RAxML_parsimony*"):
		os.remove(f)
