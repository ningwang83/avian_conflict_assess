#! /usr/bin/env python

# use this to concatenate trees with no \n at the end of file

import os,sys
print "python concatenate_treefiles.py DIRs(separate by space) with / end"

curDIR = os.getcwd()+"/"
outfile = open(curDIR+"AllMLtrees.tre","w") 
inDIR = sys.argv[1:]
for DIR in inDIR:
	for f in os.listdir(DIR):
		with open(DIR+f,"rU") as handle:
			tree = handle.readline()
			tree = tree.strip()
			outfile.write(tree+"\n")
		