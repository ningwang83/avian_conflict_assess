#! /usr/bin/env python

"""use this to order the LLSscore for each run, then copy to excel to summarize LLS for all genes
then run compare_constraintLLS.._contre.py"""

import os,sys
import tree_reader
from node import Node
from biparts import Bipart,get_biparts
import tree_utilities

def check_equal(bp1,bp2):
	inbps = False
	if bp1.equal(bp2):
		inbps = True
	return inbps

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" uniquecons.tre WORKDIR file_start"
		sys.exit(0)
	
	consuniquetrees,WORKDIR,dir_start = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	
	# read the final constraint tree and setup a list
	constraint_trees = []
	with open(consuniquetrees,"rU") as infile:
		for l in infile:
			index = l.strip().split("\t")[0]
			constre = l.strip().split("\t")[1]
			constraint_trees.append(constre)
	
	# read the file in WORKDIR and order each LLSscore.out as the order in consuniquetrees
	LLSall_orderscore = open(curDIR+"LLSallorderscore.out","w")
	for direct in os.listdir(WORKDIR):
		if not direct.startswith(dir_start): continue
		LLScoreDict = {}
		with open(WORKDIR+direct+"/all_unique_constraints.tre","rU") as infile:
			for l in infile:
				index = l.strip().split("\t")[0]
				constre = l.strip().split("\t")[1]
				LLScoreDict[constre] = int(index)
		index_list = []
		for cons in constraint_trees:
			constre = tree_reader.read_tree_string(cons)
			consbp = get_biparts(constre)[0] # get_biparts return a list, only 1 item in constraint bps, so use [0]
			for k in LLScoreDict.keys():
				ktre = tree_reader.read_tree_string(k)
				kbp = get_biparts(ktre)[0]
				equal = check_equal(kbp,consbp)
				if equal:
					index_list.append(LLScoreDict[k])
					break
		with open(WORKDIR+direct+"/LLSscore.out","rU") as handle:
			First = True
			for line in handle:
				if First: 
					First=False
					continue
				reorder_LLS = []
				genename = line.strip().split("\t")[0]
				scorelist = line.strip().split("\t")[1:-1]
				bestscore = line.strip().split("\t")[-1]
				for i in index_list:
					reorder_LLS.append(scorelist[i])
				LLSall_orderscore.write(genename+"\t"+"\t".join(reorder_LLS)+"\t"+bestscore+"\n")
	LLSall_orderscore.close()

