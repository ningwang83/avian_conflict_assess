#! /usr/bin/env python

"""
Simulating gene trees with a species tree that different edges have different population size
"""

import os,sys
import random
import dendropy# must be Dendropy 4.2.0
from dendropy.calculate import treecompare


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" truetreefile MLtreeDIR outfile"
		sys.exit(0)
	truetree,MLtreeDIR,outname = sys.argv[1:]
	outfile = open(outname,"w")
	true_gene_tree_list = []
	ML_gene_tree_list = []
	#read str for tree_gene_tree_list
	with open(truetree,"rU") as intree:
		for line in intree:
			treestr = line.strip()[5:].replace("_1","")
			true_gene_tree_list.append(treestr)
	#read tree str for ML gene tree list
	for i in range(1,501):
		treename = MLtreeDIR+"/MLgene"+str(i)+".tre"
		with open(treename,"rU") as mltre:
			mlstr = mltre.readline().strip()
			ML_gene_tree_list.append(mlstr)
			
	#read trees and compare them using RF distance
	for index in range(0,500):
		true = true_gene_tree_list[index]
		ML = ML_gene_tree_list[index]
		tns = dendropy.TaxonNamespace()
		tree1 = dendropy.Tree.get(
				data=true,
				schema="newick",
				taxon_namespace=tns)
		tree2 = dendropy.Tree.get(
				data=ML,
				schema="newick",
				taxon_namespace=tns)
		tree1.encode_bipartitions()
		tree2.encode_bipartitions()
		dist = treecompare.symmetric_difference(tree1, tree2)
		outfile.write(str(dist)+"\n")
		
	outfile.close()