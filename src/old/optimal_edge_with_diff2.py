#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts
import glob
from assessLLS_for_each_edge_on_completedata import *
from compare_constraintLLS_and_generate_contre import *

def get_confs(bp,bps): # return bipartitions, also return equal one
	conflicts = []
	for i in bps:
		if i.conflict(bp):
			conflicts.append(i)
		elif i.equal(bp):
			conflicts.append(i)
	return conflicts
def getbp(contre):
	root = tree_reader.read_tree_string(contre)
	bplist = get_biparts(root)
	conbp = bplist[0]
	return conbp

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" testing_constraints diffscorecutoff LLSscore_list num/score(N/S)"
		print "num/score indicate you want to rank the cons based on diff scores or the number of genes"
		sys.exit(0)
	consfile,cutoff,scorefile,choice = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	cutoff = float(cutoff)
	testcons = {}
	with open(consfile,"rU") as handle:
		for l in handle:
			connum = l.strip().split("\t")[0]
			constre = l.strip().split("\t")[1]
			testcons[int(connum)] = constre
	
	# read LLSscore_list and setup a dic
	LLSscores = {}
	bestMLscore = {}
	with open(scorefile,"rU") as infile:
		First = True
		for li in infile:
			if First:
				First = False
				continue
			genename = li.strip().split("\t")[0]
			consLLS = li.strip().split("\t")[1:-1]
			bestML = li.strip().split("\t")[-1]
			LLSS = [float(i) for i in consLLS]
			num = range(184)
			consdic = dict(zip(num,LLSS))
			LLSscores[genename] = consdic
			bestMLscore[genename] = bestML
	
	# for each gene, identify the bestone, compare the bestone versus the second conflict bestone, added up
	# diffscore > cutoff, rank the bestones based on sum of diffscore
	bestcons = {}
	numcons = {} # do not use number, low resolution, multiple with the same num
	NewLLSfile = open(curDIR+"newLLS.out","w")
	for gene in LLSscores.keys():
		dic = LLSscores[gene]
		sortdic = sorted(dic.items(), key=lambda x: x[1], reverse=True) # the dic becomes a list with sets in it
		sortkey = sorted(dic.items(), key=lambda x: x[0])
		first = sortdic[0]
		bestone = first[0]
		bestcon = testcons[bestone]
		bestbp = getbp(bestcon)
		bestscore = first[1]
		for k in sortdic[1:]:
			com_one = k[0]
			com_con = testcons[com_one]
			com_bp = getbp(com_con)
			if com_bp.conflict(bestbp) or k == sortdic[-1]:
				sedconfscore = k[1]
				diffscore = bestscore-sedconfscore
				#print diffscore
				if diffscore < cutoff:
					diffscore = 0
					count = 0
				else:
					count = 1
					sortscore = [str(v[1]) for v in sortkey]
					NewLLSfile.write(gene+"\t"+"\t".join(sortscore)+"\t"+bestMLscore[gene]+"\n")
				if  not bestcon in bestcons:
					bestcons[bestcon] = diffscore
					numcons[bestcon] = count
				else:
					bestcons[bestcon] += diffscore
					numcons[bestcon] += count		
				break
	# sorted the Best_trees dictionary based on tree scores and make a consensus tree
	if choice == "N":
		Dic_list = sorted(numcons.items(), key=lambda x: x[1], reverse=True)		
	else:
		Dic_list = sorted(bestcons.items(), key=lambda x: x[1], reverse=True)
	print Dic_list
	First = True
	for tre in Dic_list:
		if tre[1] == 0: continue
		root = tree_reader.read_tree_string(tre[0])
		if First:
			maintree = root
			First = False
		else:
			consbp = get_biparts(root)[0]
			maintreebps = get_biparts(maintree)
			if conflictbp(consbp,maintreebps): continue
			if check_bp_in_bps(consbp,maintreebps): continue
			#print tre[0]
			leftclade = consbp.left
			rightclade = consbp.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if not mrca_right == maintree: 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			mrca.add_child(nd)
	with open(curDIR+"consensus.tre","w") as outfile:
		outfile.write(maintree.get_newick_repr()+";\n")

			