#! /usr/bin/env python

import os,sys
import tree_reader
from node import Node

import random
import dendropy# must be Dendropy 4.2.0
from dendropy.simulate import treesim
from dendropy.model import reconcile
from dendropy.interop import seqgen
from dendropy.datamodel.charmatrixmodel import CharacterMatrix
import glob
import subprocess
import numpy as np


if __name__ == "__main__":
	if len(sys.argv) != 7:
		print "python "+sys.argv[0]+" WORKDIR consensus.tree outtreename numgenes lengthscale GC"
		print "length scale is 10000, GC = 0.39 for intron or 0.49 for exon, or in between for test"
		sys.exit(0)
	WORKDIR, consentre,speciestre,numgenes,scale,GCcontent = sys.argv[1:]
	GCcontent = float(GCcontent)
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	
	# read consensus tree and change branch length to 1/curlength
	treefile = open(consentre,"rU")
	treestr = treefile.readline()
	root = tree_reader.read_tree_string(treestr)
	for node in root.iternodes():
		if node.istip:
			node.length = 1
		else:
			if node.length == 0:
				node.length == 0
			else:
				LLSlength = node.length
					if LLSlength <= 10000:
						node.length = 5/float(LLSlength) * int(scale)
					else:
						node.length = 0.5/float(LLSlength) * int(scale)
	with open(WORKDIR+speciestre,"w") as infile:
		infile.write(root.get_newick_repr(True)+";")
	
	# read species tree and simulate gene trees
	t = dendropy.Tree.get_from_path(WORKDIR+speciestre,'newick')
	print(t.as_ascii_plot(plot_metric='length'))
	genes_to_species = dendropy.TaxonNamespaceMapping.create_contained_taxon_mapping(
						containing_taxon_namespace=t.taxon_namespace,
						num_contained=1)
	simtrees = open(WORKDIR+"simtrees."+scale,"w")	
	for j in range(int(numgenes)):
		gene_tree = treesim.contained_coalescent_tree(containing_tree=t,
		gene_to_containing_taxon_map=genes_to_species)
		simtrees.write(gene_tree.as_string(schema='newick'))
	simtrees.close()
	
	"""
	# read simtrees and generate gene_data alignment (800-3000bp)
	# from Dendropy Github find the seqgen.py file to get all the defined function
	gene_trees = dendropy.TreeList.get(path=WORKDIR+"simtrees."+scale,schema="newick")
	count = 1
	for tre in gene_trees:
		# calculate GC content, varies among genes
		a = np.random.normal(GCcontent,0.03,1) # bird exon GC content could range from 43-55%. 0.39 for intron, 0.49 for exon
		baseGC = round(a[0],2)
		b = round(baseGC/2.0,2)
		c = np.random.normal(b,0.015,1)
		baseC = round(c[0],2)
		baseG = baseGC-baseC
		baseAT = 1.0-baseGC 
		d = round(baseAT/2.0,2)
		e = np.random.normal(d,0.015,1)
		baseA = round(e[0],2)
		baseT = baseAT-baseA
		print baseA,baseC,baseG,baseT
		
		# generate alignments
		s = seqgen.SeqGen()
		s.scale_branch_lens = 1.0/float(scale) # modified properly
		s.char_model = seqgen.SeqGen.GTR
		s.gamma_shape = random.uniform(0.4,0.7)
		s.gamma_cats = 4
		s.state_freqs = [baseA, baseC, baseG, baseT] #A #C #G #T
		s.general_rates = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
		s.seq_len = random.randint(800,3000)
		d1 = s.generate(tre)
		#print(len(d1.char_matrices))
		seq = d1.char_matrices[0]
		with open(WORKDIR+"SeqGen"+str(count),"w") as outfile:
			outfile.write(seq.as_string("fasta"))
		count += 1
	"""
