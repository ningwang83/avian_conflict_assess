#! /usr/bin/env python

import os,sys
import tree_reader
from node import Node
from biparts import Bipart,get_biparts
import tree_utilities

def conflictbp(bp,bps):
	conflict = False
	for i in bps:
		if bp.conflict(i):
			conflict = True
			break
	return conflict

def check_equal(bp1,bp2):
	inbps = False
	if bp1.equal(bp2):
		inbps = True
	return inbps

def check_bp_in_bps(bp,bps):
	inbps = False
	for i in bps:
		if i.equal(bp):
			inbps = True
			break
	return inbps


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" uniquecons.tre consensus.tre WORKDIR dir_start"
		sys.exit(0)
	
	consuniquetrees,consenstre,WORKDIR,dir_start = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	WORKDIR = os.path.abspath(WORKDIR)+"/"
	
	
	# read all_unique_constraints.tre and LLSscore.out in each directory
	geneLLS = {}
	bestSum = 0
	for direct in os.listdir(WORKDIR):
		if not direct.startswith(dir_start): continue
		
		# read uniqueconstraint trees, set up a list for constraint tree 
		constraint_trees = []
		with open(WORKDIR+direct+"/all_unique_constraints.tre","rU") as infile:
			for l in infile:
				index = l.strip().split("\t")[0]
				constre = l.strip().split("\t")[1]
				constraint_trees.append(constre)
	
		# read the LLSsocre in each run and write out into the geneLLS {gene:{constree:score}} dictionary
		with open(WORKDIR+direct+"/LLSscore.out","rU") as handle:
			First = True
			for line in handle:
				if First: 
					First=False
					continue
				genename = line.strip().split("\t")[0]
				geneLLS[genename] = {}
				scorelist = line.strip().split("\t")[1:-1]
				bestscore = line.strip().split("\t")[-1]
				bestSum += float(bestscore)
				for i in range(len(scorelist)):
					geneLLS[genename][constraint_trees[i]] = float(scorelist[i])
	
	# read the specific unique constraint tree file, and order the geneLLS write to a final LLS
	sort_genes = sorted(geneLLS.keys())
	outLLS = open(curDIR+"LLS_ordered_all.out","w")
	outLLS.write("\t".join(sort_genes)+"\n")
	SumconsLLS = {}
	with open(consuniquetrees,"rU") as infile:
		count = 0
		for li in infile:
			outLLS.write("cons"+str(count)+"\t")
			cons_str = li.strip().split("\t")[1]
			constre = tree_reader.read_tree_string(cons_str)
			consbp = get_biparts(constre)[0] # get_biparts return a list, only 1 item in constraint bps, so use [0]
			sumscore = 0
			score_list = []
			for gene in sort_genes:
				for cons in geneLLS[gene].keys():
					tre = tree_reader.read_tree_string(cons)
					bp = get_biparts(tre)[0]
					inbp = check_equal(bp,consbp)
					if inbp:
						score = geneLLS[gene][cons]
						sumscore += score
						score_list.append(str(score))
						break
			count += 1
			outLLS.write("\t".join(score_list)+"\n")
			SumconsLLS[cons_str] = sumscore
	outLLS.close()
	# sorted the Best_trees dictionary based on tree scores and make a consensus tree
	Dic_list = sorted(SumconsLLS.items(), key=lambda x: x[1], reverse=True)
	#print Dic_list	
	First = True
	for tre in Dic_list:
		root = tree_reader.read_tree_string(tre[0])
		if First:
			maintree = root
			for i in maintree.iternodes():
				if i == maintree: continue
				if i.istip: continue
				i.length = float(bestSum)-tre[1]
			First = False
		else:
			consbp = get_biparts(root)[0]
			maintreebps = get_biparts(maintree)
			if conflictbp(consbp,maintreebps): continue
			if check_bp_in_bps(consbp,maintreebps): continue
			print tre[0]
			leftclade = consbp.left
			rightclade = consbp.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if not mrca_right == maintree: 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			nd.length = float(bestSum)-tre[1]
			mrca.add_child(nd)
		#print maintree.get_newick_repr(True)+";"
	with open(curDIR+consenstre,"w") as outfile:
		outfile.write(maintree.get_newick_repr(True)+";\n")
