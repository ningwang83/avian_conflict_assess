#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts
import glob
from assessLLS_for_each_edge_on_completedata import *
from compare_constraintLLS_and_generate_contre import *

def get_confs(bp,bps): # return bipartitions, also return equal one
	conflicts = []
	for i in bps:
		if i.conflict(bp):
			conflicts.append(i)
		elif i.equal(bp):
			conflicts.append(i)
	return conflicts
"""
def get_bestone_and_diffs(numlist,allcons):
	focalcons = []
	for i in numlist:
		#print i
		score = float(allcons[i])
		focalcons.append(score)
	ordercons = sorted(focalcons)
	#print ordercons
	diff = ordercons[-1] - ordercons[-2]
	if diff < 2:
		diff = 0 # not contribute for final score sumup
	ind = focalcons.index(ordercons[-1])
	cnum = numlist[ind]
	return cnum,diff
"""	
def getbp(contre):
	root = tree_reader.read_tree_string(contre)
	bplist = get_biparts(root)
	conbp = bplist[0]
	return conbp

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" testing_constraints diffscorecutoff LLSscore_list focaledge_out"
		sys.exit(0)
	consfile,cutoff,scorefile,outname = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	cutoff = float(cutoff)
	#generate testing bps from testing_constraints
	#testbps = {}
	testcons = {}
	with open(consfile,"rU") as handle:
		for l in handle:
			connum = l.strip().split("\t")[0]
			constre = l.strip().split("\t")[1]
			testcons[int(connum)] = constre
			#root = tree_reader.read_tree_string(constre)
			#bplist = get_biparts(root)
			#consbp = bplist[0]
			#testbps[consbp] = int(connum)
	# read species tree and build 48 focal edges with conflict constraints
	"""
	speciestre_bps = generate_bp(speciestre)
	allbps = testbps.keys()
	focaledges = {} ##
	count = 0
	outf = open(curDIR+outname,"w")
	for bp in speciestre_bps:
		focalcon = get_cons_from_bp(bp)
		confbps = get_confs(bp,allbps)
		focedge_num = []
		for b in confbps:
			focedge_num.append(testbps[b])
		if len(focedge_num) > 1: # there is a bipartition has all 
			focaledges["edge"+str(count)] = focedge_num
			outf.write(str(count)+"\t"+focalcon+"\n")
		count += 1
	outf.close()
	"""
	# read LLSscore_list and setup a dic
	LLSscores = {}
	with open(scorefile,"rU") as infile:
		for li in infile:
			genename = li.strip().split("\t")[0]
			consLLS = li.strip().split("\t")[1:-1]
			LLSS = [float(i) for i in consLLS]
			num = range(184)
			consdic = dict(zip(num,LLSS))
			#print consLLS
			LLSscores[genename] = consdic
	"""
	# for each local edge, each support cons, the sum of gene LLS (by only considering genes of delta diff > 2)
	rankingcons = {}
	for edge in focaledges.keys():
		sumLLS_in_edge = {}
		Cnumlist = focaledges[edge]
		for gene in LLSscores.keys():
			#print LLSscores[gene]
			cnum,diff = get_bestone_and_diffs(Cnumlist,LLSscores[gene])
			if not cnum in sumLLS_in_edge.keys():
				sumLLS_in_edge[cnum] = diff
			else:
				sumLLS_in_edge[cnum] += diff
		max = 0
		bestcons = ""
		for i in sumLLS_in_edge.keys():
			if sumLLS_in_edge[i] > max:
				max  = sumLLS_in_edge[i]
				bestcons = testcons[i]
		if not bestcons in rankingcons.keys():
			rankingcons[bestcons] = max
		else:
			if rankingcons[bestcons] < max:
				rankingcons[bestcons] = max
			else: continue"""
	
	# for each gene, identify the bestone, compare the bestone versus the second conflict bestone, added up
	# diffscore > cutoff, rank the bestones based on sum of diffscore
	bestcons = {}
	for gene in LLSscores.keys():
		dic = LLSscores[gene]
		sortdic = sorted(dic.items(), key=lambda x: x[1], reverse=True) # the dic becomes a list with sets in it
		first = sortdic[0]
		bestone = first[0]
		bestcon = testcons[bestone]
		bestbp = getbp(bestcon)
		bestscore = first[1]
		for k in sortdic[1:]:
			com_one = k[0]
			com_con = testcons[com_one]
			com_bp = getbp(com_con)
			if not com_bp.conflict(bestbp): continue
			sedconfscore = k[1]
			diffscore = bestscore-sedconfscore
			print diffscore
			if diffscore < cutoff:
				diffscore = 0
			if  not bestcon in bestcons:
				bestcons[bestcon] = diffscore
			else:
				bestcons[bestcon] += diffscore
			break
	# sorted the Best_trees dictionary based on tree scores and make a consensus tree
	print bestcons.keys()
	Dic_list = sorted(bestcons.items(), key=lambda x: x[1], reverse=True)
	#print Dic_list	
	First = True
	for tre in Dic_list:
		root = tree_reader.read_tree_string(tre[0])
		if First:
			maintree = root
			First = False
		else:
			consbp = get_biparts(root)[0]
			maintreebps = get_biparts(maintree)
			if conflictbp(consbp,maintreebps): continue
			if check_bp_in_bps(consbp,maintreebps): continue
			#print tre[0]
			leftclade = consbp.left
			rightclade = consbp.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if not mrca_right == maintree: 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			#nd.length = float(geneMLtrees_sumLLS)-tre[1]
			mrca.add_child(nd)
		#print maintree.get_newick_repr(True)+";"
	with open(curDIR+"consensus.tre","w") as outfile:
		#outfile.write(maintree.get_newick_repr(True)+";\n")
		outfile.write(maintree.get_newick_repr()+";\n")

			