#! /usr/bin/env python

"""simulated gene data contain space in sequence names, replace it with 
_, remove 1 from the name, Charadrius vociferus 1"""

import os,sys

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict



if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" inDIR outDIR"
		sys.exit(0)
	inDIR,outDIR = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	
	for f in os.listdir(inDIR):
		if not f.startswith("SeqGen"): continue
		seqDict = read_fasta(inDIR+f)
		outfile = open(outDIR+f,"w")
		for key in seqDict.keys():
			#namelist = key.split(" ")[:-1]
			#name = "_".join(namelist)
			name = key.replace(" ","_")
			outfile.write(">"+name+"\n"+seqDict[key]+"\n")
		outfile.close()
