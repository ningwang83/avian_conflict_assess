import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "python "+sys.argv[0]+" file"
        sys.exit(0)

    a = open(sys.argv[1],"r")
    header = a.readline().strip().split(" ")[1:]
    vals = {}
    for i in a:
        spls = i.strip().split(" ")
        nums = [float(j) for j in spls[1:]]
        vals[spls[0]] = nums
    valscount = [0 for j in header]
    for i in vals:
        highest = -9999999999
        count = 0
        highcount = 0
        for j in vals[i]:
            if j > highest:
                highest = j
                highcount = count
            count += 1
        valscount[highcount] += 1
    a.close()
    print valscount
