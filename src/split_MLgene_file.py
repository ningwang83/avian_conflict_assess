#! /usr/bin/env python

import os,sys

inname = sys.argv[1]
Drname = sys.argv[2]
curDIR = os.getcwd()+"/"
if not os.path.exists(curDIR+Drname):
	os.makedirs(curDIR+Drname)
with open(inname,"rU") as infile:
	gene = 1
	for l in infile:
		outname = Drname+"/MLgene"+str(gene)+".tre"
		with open(outname,"w") as outfile:
			outfile.write(l)
		outfile.close()
		gene += 1