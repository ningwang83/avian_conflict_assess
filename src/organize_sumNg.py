#! /usr/bin/env python

import os,sys
import csv

if len(sys.argv) != 3:
	print "python "+sys.argv[0]+" csvfile outname"
	sys.exit(0)

csvfile,outname = sys.argv[1:]
curDIR = os.getcwd()+"/"

infile = open(curDIR+csvfile,"rU")

reader = csv.reader(infile)
header = next(reader)

title = {}
for index,column in enumerate(header):
	title[column] = index

bestcons_nonsig = {}
bestcons_sig = {}

for row in reader:
	genename = row[title["gene"]]
	bestone = row[title["bestone"]]
	difscore = row[title["confdifflnL"]]
	
	if not bestone in bestcons_nonsig.keys():
		bestcons_nonsig[bestone] = []
		bestcons_nonsig[bestone].append(genename)
	else:
		bestcons_nonsig[bestone].append(genename)
	
	if float(difscore) >= 2:
		if not bestone in bestcons_sig.keys():
			bestcons_sig[bestone] = []
			bestcons_sig[bestone].append(genename)
		else:
			bestcons_sig[bestone].append(genename)

outfile = open(curDIR+outname,"w")
outfile.write("constraint\tnonsigNg\tsigNg\n")
for key in sorted(bestcons_nonsig.keys()):
	if key in bestcons_sig.keys():
		outfile.write(key+"\t"+str(len(bestcons_nonsig[key]))+"\t"+str(len(bestcons_sig[key]))+"\n")
	else:
		outfile.write(key+"\t"+str(len(bestcons_nonsig[key]))+"\t0\n")
outfile.close()
