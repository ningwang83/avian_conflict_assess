#! /usr/bin/env python

import os,sys
import shutil


def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict


#assign outgroups
exon_intron_outgroup = ["Homo_sapiens","Alligator_mississippiensis","Chelonia_mydas","Anolis_carolinensis"]

UCE_outgroup = "Alligator_mississippiensis"

File_end = ".fasta"
#File_end = ".intron"


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python "+sys.argv[0]+" FILEDIR Filetype outDIRname"
		print "filetype = UCE or exon, or intron"
		sys.exit()
	
	fileDIR,filetype,outDIRname = sys.argv[1:]
	fileDIR = os.path.abspath(fileDIR)+"/"
	print fileDIR
	
	if not os.path.exists(fileDIR+outDIRname):
		cmd = "mkdir "+fileDIR+outDIRname
		os.system(cmd)
	#read fasta file in fileDIR
	for f in os.listdir(fileDIR):
		if not f.endswith(File_end): continue
		print f
		seqDICT = read_fasta(fileDIR+f)
		taxa_list = sorted(seqDICT.keys())
		if filetype == "UCE":
			if UCE_outgroup in taxa_list:
				taxa_list.remove(UCE_outgroup)
		else:
			for i in exon_intron_outgroup:
				if i in taxa_list:
					taxa_list.remove(i)
		if len(taxa_list) == 48:
			shutil.move(fileDIR+f, fileDIR+outDIRname+"/"+f)


