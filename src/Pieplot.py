
# coding: utf-8

# In[18]:


get_ipython().magic('matplotlib inline')
from ete2 import Tree, TreeStyle, TextFace, NodeStyle, faces, COLOR_SCHEMES

#Read in species tree and convert to ultrametric
#Match phyparts nodes to ete3 nodes
def get_phyparts_nodes(species_tree,phyparts_root): 
    
    sptree = Tree(species_tree)
    sptree.convert_to_ultrametric()

    phyparts_node_key = [line.strip("\n") for line in open(phyparts_root+".node.key")]
    subtrees_dict = {n.split()[0]:Tree(n.split()[1]+";") for n in phyparts_node_key}
    subtrees_topids = {}
    for x in subtrees_dict:
        subtrees_topids[x] = subtrees_dict[x].get_topology_id()

    for node in sptree.traverse():
        node_topid = node.get_topology_id()
        for subtree in subtrees_dict:
            if node_topid == subtrees_topids[subtree]:
                node.name = subtree
    
    return sptree,subtrees_dict,subtrees_topids

#Summarize concordance and conflict from Phyparts
def get_concord_and_conflict(phyparts_root,subtrees_dict,subtrees_topids):

    with open(phyparts_root + ".concon.tre") as phyparts_trees:
        concon_tree = Tree(phyparts_trees.readline())
        conflict_tree = Tree(phyparts_trees.readline())

    concord_dict = {}
    conflict_dict = {}

    for node in concon_tree.traverse():
        node_topid = node.get_topology_id()
        for subtree in subtrees_dict:
            if node_topid == subtrees_topids[subtree]:
                concord_dict[subtree] = node.support
    
    for node in conflict_tree.traverse():
        node_topid = node.get_topology_id()
        for subtree in subtrees_dict:
            if node_topid == subtrees_topids[subtree]:
                conflict_dict[subtree] = node.support
                
    return concord_dict, conflict_dict    
    
#Generate Pie Chart data
def get_pie_chart_data(phyparts_root,num_genes,concord_dict,conflict_dict):

    phyparts_hist = [line.strip("\n") for line in open(phyparts_root + ".hist")]
    phyparts_pies = {}
    
    for n in phyparts_hist:
        n = n.split(",")
        tot_genes = float(n.pop(-1))
        node_name = n.pop(0)[4:]
        concord_raw = float(n.pop(0))
        concord = concord_dict[node_name]
        all_conflict = conflict_dict[node_name]
    
        if len(n) > 0:
            most_conflict = max([float(x) for x in n])
        else:
            most_conflict = 0.0
    
        adj_concord = (concord/num_genes) * 100 
        adj_most_conflict = (most_conflict/num_genes) * 100
        other_conflict = (all_conflict - most_conflict) / num_genes * 100
        the_rest = (num_genes - concord - all_conflict) / num_genes * 100
    
        pie_list = [adj_concord,adj_most_conflict,other_conflict,the_rest]
        phyparts_pies[node_name] = pie_list
        
    return phyparts_pies

#mian function
species_tree = "RAxML_bipartitions.codon3part_rooted"
phyparts_root = "RAxML_phyparts_supcut50"
taxon_subst = "replacingvalues.txt"
num_genes = 1339

plot_tree, subtrees_dict, subtrees_topids = get_phyparts_nodes(species_tree, phyparts_root)
concord_dict, conflict_dict = get_concord_and_conflict(phyparts_root, subtrees_dict, subtrees_topids)
phyparts_pies= get_pie_chart_data(phyparts_root, num_genes, concord_dict, conflict_dict)

#taxa name replacement
if taxon_subst:
    taxon_dict = {line.split(",")[0]:line.split(",")[1].strip("\n") for line in open(taxon_subst)}
    for leaf in plot_tree.get_leaves():
        try:
            leaf.name = taxon_dict[leaf.name]
        except KeyError:
            print(leaf.name)
            continue

#Pie chart layout
def phyparts_pie_layout(mynode):
    if mynode.name in phyparts_pies:
        pie= faces.PieChartFace(phyparts_pies[mynode.name],
                              colors = ["blue","springgreen","red","silver"],
                              width=120, height=120)
        faces.add_face_to_node(pie,mynode,0,position="branch-right")
        
        concord_text = faces.TextFace(str(int(concord_dict[mynode.name]))+'  ',fsize=20)
        conflict_text = faces.TextFace(str(int(conflict_dict[mynode.name]))+'  ',fsize=20)
        
        faces.add_face_to_node(concord_text,mynode,0,position="branch-top")
        faces.add_face_to_node(conflict_text,mynode,0,position="branch-bottom")
    else:
        F = faces.TextFace(' '+mynode.name,fsize=20,fstyle='italic')
        faces.add_face_to_node(F,mynode,0,position="aligned")

#Plot Pie Chart
ts = TreeStyle()
ts.show_leaf_name = False
ts.layout_fn = phyparts_pie_layout
ts.draw_guiding_lines = True
ts.guiding_lines_color = "black"
ts.guiding_lines_type = 0
ts.branch_vertical_margin = 30
ts.optimal_scale_level = "full"

nstyle = NodeStyle()
nstyle["size"] = 0
for n in plot_tree.traverse():
    n.set_style(nstyle)

plot_tree.ladderize(direction=1)
#plot_tree.show(tree_style=ts)
plot_tree.render("%%inline",tree_style=ts)

