#! /usr/bin/env python

import os,sys
import tree_reader
from biparts import Bipart,get_biparts


def generate_bp(treefile):
	intree = open(treefile,"rU")
	treeline = intree.readline().strip()
	MLtree = tree_reader.read_tree_string(treeline)
	MLbps = get_biparts(MLtree)
	intree.close()
	return MLbps

def get_cons_from_bp(bp):
	left = ",".join(bp.left)
	right = ",".join(bp.right)
	cons = "(("+left+"),"+right+");"
	return cons

def check_bp_in_bps(bp,bps):
	inbps = False
	for i in bps:
		if i.equal(bp):
			inbps = True
			break
	return inbps

def get_conf_from_canditre(bp,bps):
	conflict_list = []
	for i in bps:
		if i.conflict(bp):
			conftrestr = get_cons_from_bp(i)
			conflict_list.append(conftrestr)
	return conflict_list

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" Majorspecies_tree testingtreeDIR"
		print "testingtreeDIR containts some species tree from Jarvis"
		sys.exit(0)
	speciestre,testtreDIR = sys.argv[1:]
	testtreDIR = os.path.abspath(testtreDIR)+"/"
	curDIR = os.getcwd()+"/"
		
	#generate testing bps from testing tree files
	first = True
	testbps = []
	for testtre in os.listdir(testtreDIR):
		if first:
			testbps = generate_bp(testtreDIR+testtre)
			first = False
		else:
			trebps = generate_bp(testtreDIR+testtre)
			for i in trebps:
				if check_bp_in_bps(i,testbps) == False:
					testbps.append(i)

	#read species tree and get cons0 for each edge, combine all set(constraints), no duplication
	all_constraints = []
	speciestre_bps = generate_bp(speciestre)
	count = 0
	for bp in speciestre_bps:
		conflict_list = get_conf_from_canditre(bp,testbps)
		cons0 = get_cons_from_bp(bp)
		all_constraints.append(cons0)
		all_constraints = all_constraints + conflict_list
	unique_constraints = set(all_constraints)
	with open(curDIR+"all_unique_constraints.tre", "w") as outtrefile:
		num = 0
		for uc in unique_constraints:
			outtrefile.write(str(num)+"\t"+uc+"\n")
			num += 1
	
