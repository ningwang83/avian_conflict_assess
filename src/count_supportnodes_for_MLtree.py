#! /usr/bin/env python

# read gene trees and calculate the number of nodes that support (>70%) by each gene
# use the results to make histogram

import tree_reader
import os,sys
from node import Node


if len(sys.argv) != 5:
	print "python "+sys.argv[0]+" intreeDIR fasDIR outfile supportcut"
	sys.exit(0)

treDIR,fasDIR,outname,spcutoff = sys.argv[1:]
curDIR = os.getcwd()+"/"
treDIR = os.path.abspath(treDIR)+"/"
fasDIR = os.path.abspath(fasDIR)+"/"

countDict = {} # file name: support node number
seqlenDict = {} # file name: alignment length
for tre in os.listdir(treDIR):
	if not tre.endswith("treefile"): continue
	print tre
	nodenum = 0
	filename = tre.split(".")[0]
	fastaname = tre[:-9]
	with open(fasDIR+fastaname,"rU") as handle:
		align = ""
		First = True
		for l in handle:
			if l.startswith(">") and First: 
				First = False
				continue
			elif l.startswith(">") and not First:
				seqlenDict[filename] = str(len(align))
				break
			else:
				align += l.strip()
	# read tree
	with open(treDIR+tre,"rU") as infile:
		trestr = infile.readline()
		root = tree_reader.read_tree_string(trestr)
	
	for node in root.iternodes():
		if node.istip or node == root: continue
		#print node.label
		if node.label == "": continue
		sup = int(node.label)
		if sup >= int(spcutoff):
			nodenum += 1
	countDict[filename] = str(nodenum)

with open(curDIR+outname,"w") as outfile:
	for key in countDict.keys():
		outfile.write(key+"\t"+countDict[key]+"\t"+seqlenDict[key]+"\n")