#! /usr/bin/env python

import os,sys
import tree_reader

"""UCE data has a different species name than the tree files from Jarvis, so need to change the treefiles tip
name to match UCE data to conduct the complete analyses"""


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" species.txt treeDIR/file outDIR/file DIR(y/n)"
		sys.exit(0)
	
	DIR = sys.argv[-1]
	if DIR == "y":
		treeDIR,outDIR = sys.argv[2:-1]
		treeDIR = os.path.abspath(treeDIR)+"/"
		outDIR = os.path.abspath(outDIR)+"/"
	else:
		treefile,outname = sys.argv[2:-1]
		curDIR = os.getcwd()+"/"
	# read species_match file and read in species names
	# column 1 is from UCE and column 2 is from Intron
	nameDic = {}
	with open(sys.argv[1], "rU") as handle:
		for li in handle:
			UCEname = li.strip().split("\t")[0]
			Intron_name = li.strip().split("\t")[1]
			nameDic[Intron_name] = UCEname
	
	# read tree files and change tip name to match UCE data
	if DIR == "y":
		for f in os.listdir(treeDIR):
			if not f.endswith(".tre"): continue
			intree = open(treeDIR+f,"rU")
			treeline = intree.readline().strip()
			root = tree_reader.read_tree_string(treeline)
			for lv in root.leaves():
				label = lv.label
				lv.label = nameDic[label]
			with open(outDIR+f,"w") as outfile:
				outfile.write(root.get_newick_repr(True)+";")
	else:
		outfile = open(curDIR+outname,"w")
		with open(treefile,"rU") as intree:
			for l in intree:
				#tree_index = l.strip().split("\t")[0]
				#treeline = l.strip().split("\t")[-1]
				treeline = l.strip()
				root = tree_reader.read_tree_string(treeline)
				for lv in root.leaves():
					label = lv.label
					lv.label = nameDic[label]
				#outfile.write(tree_index+"\t"+root.get_newick_repr(True)+";\n")
				outfile.write(root.get_newick_repr(True)+";")
		outfile.close()

