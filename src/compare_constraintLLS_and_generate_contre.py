#! /usr/bin/env python

import os,sys
import tree_reader
from node import Node
from biparts import Bipart,get_biparts
import tree_utilities

def conflictbp(bp,bps):
	conflict = False
	for i in bps:
		if bp.conflict(i):
			conflict = True
			break
	return conflict

def check_bp_in_bps(bp,bps):
	inbps = False
	for i in bps:
		if bp.equal(i):
			inbps = True
			break
	return inbps



if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" uniquecons.tre SumLLS.txt consensus.tre"
		sys.exit(0)
	
	consuniquetrees,sumLLS,consenstre = sys.argv[1:]
	curDIR = os.getcwd()+"/"
	
	# read sumLLS.txt file, add summed LLS into list
	# the last score is the LLS from ML gene trees
	with open(sumLLS,"rU") as handle:
		line = handle.readline().strip()
		geneMLtrees_sumLLS = line.split("\t")[-1]
		constraints_sumLLS = line.split("\t")[1:-1]
	
	# read uniqueconstraint trees, set up Dictionary with key is the constraint tree str and value is the summed LLS
	LLScoreDict = {}
	with open(consuniquetrees,"rU") as infile:
		for l in infile:
			index = l.strip().split("\t")[0]
			constre = l.strip().split("\t")[1]
			LLScoreDict[constre] = float(constraints_sumLLS[int(index)])
	
	# sorted the Best_trees dictionary based on tree scores and make a consensus tree
	Dic_list = sorted(LLScoreDict.items(), key=lambda x: x[1], reverse=True)
	#print Dic_list	
	First = True
	for tre in Dic_list:
		root = tree_reader.read_tree_string(tre[0])
		if First:
			maintree = root
			for i in maintree.iternodes():
				if i == maintree: continue
				if i.istip: continue
				i.length = float(geneMLtrees_sumLLS)-tre[1]
			First = False
		else:
			consbp = get_biparts(root)[0]
			maintreebps = get_biparts(maintree)
			if conflictbp(consbp,maintreebps): continue
			if check_bp_in_bps(consbp,maintreebps): continue
			print tre[0]
			leftclade = consbp.left
			rightclade = consbp.right
			mrca_left = tree_utilities.get_mrca_wnms(leftclade,maintree)
			mrca_right = tree_utilities.get_mrca_wnms(rightclade,maintree)
			if not mrca_right == maintree: 
				mrca = mrca_right
				clades = rightclade
			else:
				mrca = mrca_left
				clades = leftclade
			mvnds = set()
			for j in mrca.children:
				if len(set(j.lvsnms()).intersection(clades)) > 0:
					mvnds.add(j)
			nd = Node()
			for j in mvnds:
				mrca.remove_child(j)
				nd.add_child(j)
			nd.length = float(geneMLtrees_sumLLS)-tre[1]
			mrca.add_child(nd)
		#print maintree.get_newick_repr(True)+";"
	with open(curDIR+consenstre,"w") as outfile:
		#outfile.write(maintree.get_newick_repr(True)+";\n")
		outfile.write(maintree.get_newick_repr()+";\n")
