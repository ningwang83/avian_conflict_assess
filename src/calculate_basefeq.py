#! /usr/bin/env python

"""use this script to calculate the average ATGC% for each gene data type
and build a GC content density plot using R or pygal. we ignore ambiguity sites"""

import os,sys
import glob


#file_end = ".fas"

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" fasDIR outname file_end"
		sys.exit(0)
	
	fasDIR,outname,file_end = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	curDIR = os.getcwd()+"/"
	
	GCdic = {} # key is the file name, and value is a list of GCAT content of each gene
	# read each gene fasta file and calculate the average GC% for that gene from different species
	for f in os.listdir(fasDIR):
		if not f.endswith(file_end): continue
		Atotal = 0.0
		Gtotal = 0.0
		Ttotal = 0.0
		Ctotal = 0.0	
		seqDic = read_fasta(fasDIR+f)
		for k in seqDic.keys():
			seq = seqDic[k].replace("-","").replace("?","").replace("N","").upper()
			totallen = float(len(seq))
			Afreq = round(seq.count("A")/totallen,3)
			Gfreq = round(seq.count("G")/totallen,3)
			Tfreq = round(seq.count("T")/totallen,3)
			Cfreq = round(seq.count("C")/totallen,3)
			Atotal += Afreq
			Gtotal += Gfreq
			Ttotal += Tfreq
			Ctotal += Cfreq
		Aavefreq = round(Atotal/len(seqDic.keys()),3)
		Gavefreq = round(Gtotal/len(seqDic.keys()),3)
		Tavefreq = round(Ttotal/len(seqDic.keys()),3)
		Cavefreq = round(Ctotal/len(seqDic.keys()),3)
		GCfreq = Gavefreq+Cavefreq
		ATfreq = Aavefreq+Tavefreq
		GCdic[f] = [str(Aavefreq),str(Gavefreq),str(Tavefreq),str(Cavefreq),str(GCfreq),str(ATfreq)]
	
	outfile = open(curDIR+outname,"w")
	outfile.write("filename,Afreq,Gfreq,Tfreq,Cfreq,GC,AT\n")
	for file in GCdic.keys():
		outfile.write(file+","+",".join(GCdic[file])+"\n")
	outfile.close()