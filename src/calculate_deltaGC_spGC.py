#! /usr/bin/env python

# use this to calculate GC variation among taxa and among genes

import os,sys
import numpy as np

# exclude outgroups to decrease variation
exclude = ["Alligator_mississippiensis","Chelonia_mydas","Anolis_carolinensis","Homo_sapiens"]

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

def remove_uninf(seqDict): # read in aligned sequences
	alignid = sorted(seqDict.keys())
	alignlen = len(seqDict[alignid[0]])
	align = []
	for i in alignid:
		seq = seqDict[i]
		align.append(seq)
	uninf = []
	for j in range(alignlen):
		column = []
		for a in align:
			if a[j] != "-" and a[j] != "?" and a[j] != "N":
				column.append(a[j])
		if len(set(column)) == 1:
			uninf.append(j)
	#print uninf
	newaln = []
	for al in align:
		newseq = ""
		for num in range(len(al)):
			if not num in uninf:
				newseq += al[num]
		newaln.append(newseq)
	newdict = dict(zip(alignid,newaln))
	return newdict

def deltaGC(newseqDic):
	GCcontent = {}
	nooutgroup = {}
	for k in newseqDic.keys():
		seq = newseqDic[k].replace("-","").replace("?","").replace("N","").upper()
		totallen = float(len(seq))
		Afreq = round(seq.count("A")/totallen,3)
		Gfreq = round(seq.count("G")/totallen,3)
		Tfreq = round(seq.count("T")/totallen,3)
		Cfreq = round(seq.count("C")/totallen,3)
		GC = Gfreq+Cfreq
		GCcontent[k] = GC
		if not k in exclude:
			nooutgroup[k] = GC
	gclist = sorted(nooutgroup.values()) # exclude outgroup to avoid large variation
	Q1 = np.percentile(gclist, 25)
	Q3 = np.percentile(gclist, 75)
	P5 = np.percentile(gclist, 5)
	P95 = np.percentile(gclist, 95)
	deltagc = Q3-Q1
	gc10 = P95-P5
	rangegc = gclist[-1]-gclist[0]
	return GCcontent, deltagc, gc10, rangegc

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python "+sys.argv[0]+" fasDIR speciesgc gene_deltagc file_end"
		print "speciesgc outfile, and gene_deltagc outfile"
		sys.exit(0)
	
	fasDIR,outspGC,outDelGC,file_end = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	curDIR = os.getcwd()+"/"
	outdelta = open(curDIR+outDelGC,"w")
	outdelta.write("genename\tquatiledetlaGC\tgc10\trangeGC\n")
	speciesGC = {}
	for f in os.listdir(fasDIR):
		print f
		if not f.endswith(file_end): continue
		genename = f.split(".")[0]
		seqDict = read_fasta(fasDIR+f)
		newdict = remove_uninf(seqDict)
		# check newdict for intron data, a test for missing data effect
		"""with open(curDIR+genename+".cln.fas","w") as outfas:
			for nk in newdict:
				outfas.write(">"+nk+"\n"+newdict[nk]+"\n")"""
		GCcontent,deltagc,gc10,rangegc = deltaGC(newdict)
		outdelta.write(genename+"\t"+str(deltagc)+"\t"+str(gc10)+"\t"+str(rangegc)+"\n")
		for k in GCcontent.keys():
			if not k in speciesGC.keys():
				speciesGC[k] = [GCcontent[k]]
			else:
				speciesGC[k].append(GCcontent[k])
	outdelta.close()
	
	outspfile = open(curDIR+outspGC,"w")
	outspfile.write("species\tmedianGC\tmeanGC\tstdGC\n")
	for sp in speciesGC.keys():
		GClist = sorted(speciesGC[sp])
		median = np.median(GClist)
		mean = round(np.mean(GClist),4)
		std = round(np.std(GClist),4)
		outspfile.write(sp+"\t"+str(median)+"\t"+str(mean)+"\t"+str(std)+"\n")
	outspfile.close()
		
		
	

