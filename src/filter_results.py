#! /usr/bin/env python

import os,sys

if len(sys.argv) != 6:
	print "python filter_results.py filtergene.txt csvDIR outDIR keep(Yes/No) suffix"
	sys.exit(0)

filtgene,csvDIR,outDIR,keep,suffix = sys.argv[1:]
csvDIR = os.path.abspath(csvDIR)+"/"
if not os.path.exists(outDIR):
	os.makedirs(outDIR)
outDIR = os.path.abspath(outDIR)+"/"

# read file and write a list to filter genes
filter = []
with open(filtgene,"rU") as handel:
	for l in handel:
		locus = l.strip().split(".")[0]
		filter.append(locus)
# read csv file in the DIR and output filtered results
for csv in os.listdir(csvDIR):
	if not csv.endswith(".csv"): continue
	fname = csv.split(".csv")[0]
	outfile = open(outDIR+fname+"."+suffix+".csv","w")
	First = True
	with open(csvDIR+csv,"rU") as infile:
		for l in infile:
			if First:
				outfile.write(l)
				First = False
			else:
				locusname = l.strip().split(",")[0]
				gene = locusname.split(".")[0]
				if keep == "No":
					if not gene in filter:
						outfile.write(l)
				else:
					if gene in filter:
						outfile.write(l)
	outfile.close()