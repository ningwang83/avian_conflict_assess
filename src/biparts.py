class Bipart:
    def __init__ (self,lf,rt):
        self.left = lf
        self.right = rt
        self.union = lf.union(rt)

    def __str__(self):
        x = ",".join(list(self.left))
        y = ",".join(list(self.right))
        return x+" | "+y

    def conflict(self, inbp):
        if len(inbp.right.intersection(self.right)) > 0 and len(inbp.right.intersection(self.left)) > 0:
            if len(inbp.left.intersection(self.right)) > 0 and len(inbp.left.intersection(self.left)) > 0:
                return True
        if len(inbp.left.intersection(self.left)) > 0 and len(inbp.left.intersection(self.right)) > 0:
            if len(inbp.right.intersection(self.left)) > 0 and len(inbp.right.intersection(self.right)) > 0:
                return True
        return False
    
    def equal(self, inbp):
        inter = self.union.intersection(inbp.union)
        if self.left.intersection(inter) == inbp.left.intersection(inter) and self.right.intersection(inter)  == inbp.right.intersection(inter):
            return True
        if self.left.intersection(inter)  == inbp.right.intersection(inter) and self.right.intersection(inter)  == inbp.left.intersection(inter):
            return True
        return False

    def newick(self, exclude=None):
        if exclude == None:
            return "(("+",".join(list(self.left))+"),("+",".join(list(self.right))+"))"
        else:
            if len(self.left.difference(exclude)) < 2:
                return "DONTRUN"
            elif len(self.right.difference(exclude)) < 2:
                return "DONTRUN"
            else:
                return "(("+",".join(list(self.left.difference(exclude)))+"),("+",".join(list(self.right.difference(exclude)))+"))"

def get_biparts(rt):
    bps = []
    out = set(rt.lvsnms())
    for i in rt.iternodes():
        if len(i.children) == 0:
            continue
        if i == rt:
            continue
        if i.length < 0.0000: # use to be rt.length, probably wrong
            continue
        right = (set(i.lvsnms()))
        left = (set(out-right))
        bp = Bipart(left,right)
        bps.append(bp)
    return bps

