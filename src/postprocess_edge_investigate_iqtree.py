import sys
import os

"""
out.conflict
"""

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python "+sys.argv[0]+" out.conflict iqtreedir"
        print "the best iqtree file should be in the same directory"
        sys.exit(0)
    oc = open(sys.argv[1],"r")
    constraints = {} #key is constraint number and value is conflict alts
    curcons = None
    curlist = None
    reccon = False
    for i in oc:
        if "constraint" in i:
            reccon = False
            spls = i.strip().split(": ")[1]
            if curcons == None:
                curcons = spls
                curlist = []
            else:
                constraints[curcons] = curlist
                curlist = []
                curcons = spls
            continue
        if "-conflicts-" in i:
            reccon = True
            continue
        if reccon == True:
            spls = i.strip().split(" : ")[0]
            curlist.append(spls)
    #get last one
    constraints[curcons] = curlist
    oc.close()
    outd = sys.argv[2]
    if outd[-1] != "/":
        outd += "/"
    
    scores = {} #key is seqfname , value is dictionary with runname
    aic = {}
    aicc = {}
    analyses = set ()
    for i in os.listdir(outd):
        if "iqtree" in i:
            spl = i.split(".iqtree")
            #there is some confusion here because of whether the file name is in the end or the beginning
            #one option
            spl = spl[0].split("___")
            sfn = spl[0] # gene name
            #another option
            if sfn not in scores:
                scores[sfn] = {}
                aic[sfn] = {}
                aicc[sfn] = {}
            anafn = ""
            if len(spl) == 2:
                anafn = spl[1].replace("___","")
            analyses.add(anafn)
            print sfn,anafn
            of = open(outd+i,"r")
            for j in of:
                if "Log-likelihood of the tree" in j:
                    sc = float(j.strip().split(": ")[1].split(" ")[0])
                    scores[sfn][anafn] = sc
                elif "Akaike information criterion (AIC) score" in j:
                    sc = float(j.strip().split(": ")[1].split(" ")[0])
                    aic[sfn][anafn] = sc
                elif "Corrected Akaike information criterion (AICc) score" in j:
                    sc = float(j.strip().split(": ")[1].split(" ")[0])
                    aicc[sfn][anafn] = sc
            of.close()
    print analyses    
    for i in constraints:
        outf = open("cons_"+i+".csv","w")
        nm = "cons_"+i
        outf.write("gene,"+nm)
        for j in constraints[i]:
            nm2 = ","+nm+"_conf_"+str(j)
            outf.write(nm2)
        outf.write(",bestone,best,secondbest,diffbestsecondbest,ml\n")
        for k in scores:
            try:
                scores[k][nm]
            except:
                print k
                continue
            stri = k +","+str(scores[k][nm])
            best = scores[k][nm]
            secondbest = -9999999999
            bestone = nm
            for j in constraints[i]:
                nm2 = nm+"_conf_"+str(j)
                try:
                    stri += ","+str(scores[k][nm2])
                    if scores[k][nm2] > best:
                        secondbest = best
                        best = scores[k][nm2]
                        bestone = nm2
                    elif secondbest < scores[k][nm2] <= best:
                        secondbest = scores[k][nm2]
                        continue
                except:
                    stri += ",-"
            #stri += ","+bestone+","+str(best)+","+str(secondbest)+","+str(best-secondbest)
            #if there is an ml tree
            stri += ","+bestone+","+str(best)+","+str(secondbest)+","+str(best-secondbest)+","+str(scores[k][""])
            outf.write(stri+"\n")
        outf.close()
            
        #for j in scores:
        #    stri = j
    """
    for i in constraints:
        outf = open("cons_"+i+".out","w")
        nm = "cons_"+i
        print nm
        for j in scores:
            stri = str(j)+","+str(scores[j][nm])
            #for k in constraints[i]:
                #print j, scores[i][j]
            #    print j
        outf.close()
    """
